//
//  RequestManager.swift
//  Bebarobeshoor
//
//  Created by Developer on 13/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Foundation
import MBProgressHUD
import AFNetworking
import Localize_Swift

let isLive = true

let kAPISignUp = isLive ? "api_signup" : "signup"
let kAPILogin = isLive ? "api_login" : "login"
let kAPIForgotPassword = isLive ? "api_forgot_password" : "forgot_password"
let kAPIProductList = isLive ? "api_products" : "products"
let kAPIPackages = isLive ? "api_packages" : "packages"
let kAPIDates = isLive ? "api_dates" : "dates"
let kAPIHours = isLive ? "api_hours" : "hours"
let kAPIOrder = isLive ? "api_order" : "order"
let kAPIProfile = isLive ? "api_profile" : "profile"
let kAPIOrderStatus = isLive ? "api_order_status" : "order_status"
let kAPIReferral = isLive ? "api_referral" : "referral"
let kAPIEditProfile = isLive ? "api_editprofile" : "editprofile"
let kAPIPayments = isLive ? "api_social_networks.php" : "social_networks.php"
let kAPIOrderCancel = isLive ? "api_delete_order" : "delete_order"

//http://bebarobeshoor.com/sms/smsapi.php?mobile=09124587504

//NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://epaybank.ir/secure_gateway/social_networks.php?mobile=09141776345&price=50000"]];

class RequestManager : NSObject {
    
    static var singleton = RequestManager()
    
    var kBaseURL = isLive ? "https://bebarobeshoor.com/index.php/" : "http://mylivecode.com/codeigniter/index.php/"
    
    let messageNoIntenetConnection = "بدون اتصال به اینترنت".localized()
    let messageParsingError = "بدون پاسخ معتبری"
    let messageNoRecordFound = "موردی یافت"
    
    var apiManager : AFHTTPSessionManager {
        
        let manager = AFHTTPSessionManager()
        
        //let requestSerializer = AFJSONRequestSerializer()
        //requestSerializer.stringEncoding = String.Encoding.utf8.rawValue
        //manager.requestSerializer = requestSerializer
        manager.responseSerializer = AFJSONResponseSerializer()
        
        let codeToAccept = NSMutableIndexSet()
        codeToAccept.add(400)
        codeToAccept.add(200)
        codeToAccept.add(201)
        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"];
        manager.responseSerializer.acceptableStatusCodes = codeToAccept as IndexSet
        
        return manager
    }
    
    func requestGET(requestMethod : String, parameters : [String: AnyObject], showLoader : Bool, successBlock:@escaping ((_ response : NSDictionary)->Void), failureBlock:@escaping ((_ response : NSDictionary?, _ error : Error?) -> Void)) {
        
        if !AFNetworkReachabilityManager.shared().isReachable {
            messageBox(message: messageNoIntenetConnection)
            failureBlock(nil, nil)
            return
        }
        
        //let requestURL = kBaseURL.appending(requestMethod)
        let parameterString = parameters.stringFromHttpParameters()
        let requestURL = URL(string:"\(kBaseURL)\(requestMethod)?\(parameterString)")!
        
        debugPrint("------------- Start -------------");
        print("requestURL :- \(requestURL)");
        
        //let escapedString = requestURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        let escapedString = requestURL.absoluteString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        
        let window = AppDelegate.singleton.window?.rootViewController
        var hud = MBProgressHUD()
        
        if showLoader && window != nil {
            
            hud = MBProgressHUD.showAdded(to: window!.view, animated: true)
            hud.mode = .indeterminate
            hud.label.text = "بارگیری..";
            hud.animationType = .zoomIn
            hud.tintColor = UIColor.darkGray
            hud.contentColor = UIColor.black
        }
        
        let manager = apiManager
        
        manager.get(escapedString!, parameters: nil,progress: { (data) in
            
        }, success: { (dataTask, response) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            
            if let data = (response as? NSDictionary)?.object(forKey: "response") as? NSDictionary {
                
                print("response :- \(data)");
                
                debugPrint("------------- End -------------");
                
                if (data.value(forKeyPath: "code") as! String) == "200" {
                    successBlock(data)
                } else {
                     failureBlock(data, nil)
                }
            } else if let data = (response as? NSArray) {
                
                print("response :- \(data)");
                
                debugPrint("------------- End -------------");
                
                if data.count > 0 {
                    
                    let dict = NSDictionary(dictionary: [requestMethod : data])
                    successBlock(dict)
                    
                } else {
                    
                    let dict = NSDictionary(dictionary: [requestMethod : []])
                    failureBlock(dict, nil)
                }
                
            } else {
                messageBox(message: self.messageParsingError)
            }
            
        }) { (dataTask, error) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            messageBox(message: "درخواست زمان")
            print("error :- \(error.localizedDescription)");
            debugPrint("------------- End -------------");
            failureBlock(nil, error)
        }
    }
    

    func requestSendSMS(requestString : String, showLoader : Bool, successBlock:@escaping ((_ response : NSDictionary)->Void), failureBlock:@escaping ((_ response : NSDictionary?, _ error : Error?) -> Void)) {
        
        if !AFNetworkReachabilityManager.shared().isReachable {
            messageBox(message: messageNoIntenetConnection)
            failureBlock(nil, nil)
            return
        }
        
        let requestURL = URL(string: requestString)!
        
        debugPrint("------------- Start -------------");
        print("requestURL :- \(requestURL)");
        
        //let escapedString = requestURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        let escapedString = requestURL.absoluteString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        
        let window = AppDelegate.singleton.window?.rootViewController
        var hud = MBProgressHUD()
        
        if showLoader && window != nil {
            
            hud = MBProgressHUD.showAdded(to: window!.view, animated: true)
            hud.mode = .indeterminate
            hud.label.text = "بارگیری..";
            hud.animationType = .zoomIn
            hud.tintColor = UIColor.darkGray
            hud.contentColor = UIColor.black
        }
        
        let manager = apiManager
        
        manager.get(escapedString!, parameters: nil,progress: { (data) in
            
        }, success: { (dataTask, response) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            
            if let data = (response as? NSDictionary)?.object(forKey: "response") as? NSDictionary {
                
                print("response :- \(data)");
                
                debugPrint("------------- End -------------");
                
                if (data.value(forKeyPath: "code") as? String) == "200" {
                    successBlock(data)
                } else {
                    failureBlock(data, nil)
                }
            } else if let data = (response as? NSArray) {
                
                print("response :- \(data)");
                
                debugPrint("------------- End -------------");
                
                if data.count > 0 {
                    
                    let dict = NSDictionary(dictionary: ["response" : data])
                    successBlock(dict)
                    
                } else {
                    
                    let dict = NSDictionary(dictionary: ["response" : []])
                    failureBlock(dict, nil)
                }
                
            } else {
                messageBox(message: self.messageParsingError)
            }
            
        }) { (dataTask, error) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            messageBox(message: "درخواست زمان")
            print("error :- \(error.localizedDescription)");
            debugPrint("------------- End -------------");
            failureBlock(nil, error)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Initialisation
    
    override init() {
        super.init()
        
        //AFNetworkReachabilityManager.shared().startMonitoring()
    }
}

extension String {
    
    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
    
}

extension Dictionary {
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).addingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).addingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
    
}
