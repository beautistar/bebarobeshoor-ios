//
//  ProductManager.swift
//  Bebarobeshoor
//
//  Created by Dharmesh on 18/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class ProductManager: NSObject {
    
    var products : [ProductModal] = []
    
    static var singleton = ProductManager()
    
    func addProduct(product : ProductModal) {
        
        products = products.map { (indexProduct : ProductModal) -> ProductModal in
            
            if indexProduct.idProduct == product.idProduct {
                return product
            } else {
                return indexProduct
            }
        }
        
        if !products.contains(product) {
            products.append(product)
        }
    }
    
    func removeProduct(product : ProductModal) {
        
        products = products.map { (indexProduct : ProductModal) -> ProductModal in
            
            if indexProduct.idProduct == product.idProduct {
                return product
            } else {
                return indexProduct
            }
        }
        
        if products.contains(product) {
            let index = products.index(of: product)
            if index != nil {
                products.remove(at: index!)
            }
        }
    }
}
