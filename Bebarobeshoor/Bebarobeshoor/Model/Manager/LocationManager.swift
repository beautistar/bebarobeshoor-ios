//
//  LocationManager.swift
//  Bebarobeshoor
//
//  Created by Dharmesh on 24/02/17.
//  Copyright © 2017 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

@objc
protocol LocationManagerDelegate {
    
    @objc func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
}

class LocationManager : NSObject, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var delegate : LocationManagerDelegate?
    var coordinate2D : CLLocationCoordinate2D! = CLLocationCoordinate2D()
    
    static var singleton = LocationManager()
    
    //MARK: Memory Management Method
    
    deinit { //same like dealloc in ObjectiveC
        
    }
   
    //------------------------------------------------------
    
    //MARK: Public
    
    func startMonitoring() {
        locationManager.startUpdatingLocation()
    }
    
    func stopMonitoring() {               
        locationManager.stopUpdatingLocation()
    }
    
    //------------------------------------------------------
    
    //MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (locations.first != nil) {
            coordinate2D = locations.first!.coordinate
            delegate?.locationManager(manager, didUpdateLocations: locations)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Initialisation
    
    override init() {
        super.init()
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
    }
    
    //------------------------------------------------------
}
