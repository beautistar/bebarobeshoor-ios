//
//	RootClass.swift
//
//	Create by Dharmesh Avaiya on 18/2/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class MessageProductModal : NSObject, NSCoding{

	var products : [ProductModal]!
    var packages : [PackageModal]!
    var hours : [HoursModal]!
    var dates : [DatesModal]!
    
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		products = [ProductModal]()
		if let responseArray = dictionary[kAPIProductList] as? [NSDictionary]{
			for dic in responseArray{
				let value = ProductModal(fromDictionary: dic)
				products.append(value)
			}
		}
        
        packages = [PackageModal]()
        if let responseArray = dictionary[kAPIPackages] as? [NSDictionary]{
            for dic in responseArray{
                let value = PackageModal(fromDictionary: dic)
                packages.append(value)
            }
        }
        
        hours = [HoursModal]()
        if let responseArray = dictionary[kAPIHours] as? [NSDictionary]{
            for dic in responseArray{
                let value = HoursModal(fromDictionary: dic)
                hours.append(value)
            }
        }
        
        dates = [DatesModal]()
        if let responseArray = dictionary[kAPIDates] as? [NSDictionary]{
            for dic in responseArray{
                let value = DatesModal(fromDictionary: dic)
                dates.append(value)
            }
        }
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		
        if products != nil{
			var dictionaryElements = [NSDictionary]()
			for responseElement in products {
				dictionaryElements.append(responseElement.toDictionary())
			}
			dictionary[kAPIProductList] = dictionaryElements
		}
        
        if packages != nil{
            var dictionaryElements = [NSDictionary]()
            for responseElement in packages {
                dictionaryElements.append(responseElement.toDictionary())
            }
            dictionary[kAPIPackages] = dictionaryElements
        }
        
        if hours != nil {
            var dictionaryElements = [NSDictionary]()
            for responseElement in hours {
                dictionaryElements.append(responseElement.toDictionary())
            }
            dictionary[kAPIHours] = dictionaryElements
        }
        
        if dates != nil {
            var dictionaryElements = [NSDictionary]()
            for responseElement in dates {
                dictionaryElements.append(responseElement.toDictionary())
            }
            dictionary[kAPIDates] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        products = aDecoder.decodeObject(forKey: kAPIProductList) as? [ProductModal]
        packages = aDecoder.decodeObject(forKey: kAPIPackages) as? [PackageModal]
        hours = aDecoder.decodeObject(forKey: kAPIHours) as? [HoursModal]
        dates = aDecoder.decodeObject(forKey: kAPIDates) as? [DatesModal]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if products != nil{
			aCoder.encode(products, forKey: kAPIProductList)
		}
        
        if packages != nil{
            aCoder.encode(packages, forKey: kAPIPackages)
        }
        
        if hours != nil{
            aCoder.encode(hours, forKey: kAPIHours)
        }
        
        if dates != nil{
            aCoder.encode(dates, forKey: kAPIDates)
        }
	}
}
