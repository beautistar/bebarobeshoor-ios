//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class MessageOrderModal : NSObject, NSCoding {

	var code : String!
	var msg : String!
	var orderDetails : [OrderDetailModal]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		code = dictionary["code"] as? String
		msg = dictionary["msg"] as? String
		orderDetails = [OrderDetailModal]()
		if let orderDetailsArray = dictionary["order_details"] as? [NSDictionary]{
			for dic in orderDetailsArray{
				let value = OrderDetailModal(fromDictionary: dic)
				orderDetails.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if code != nil{
			dictionary["code"] = code
		}
		if msg != nil{
			dictionary["msg"] = msg
		}
		if orderDetails != nil{
			var dictionaryElements = [NSDictionary]()
			for orderDetailsElement in orderDetails {
				dictionaryElements.append(orderDetailsElement.toDictionary())
			}
			dictionary["order_details"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         code = aDecoder.decodeObject(forKey: "code") as? String
         msg = aDecoder.decodeObject(forKey: "msg") as? String
         orderDetails = aDecoder.decodeObject(forKey: "order_details") as? [OrderDetailModal]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if code != nil{
			aCoder.encode(code, forKey: "code")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
		if orderDetails != nil{
			aCoder.encode(orderDetails, forKey: "order_details")
		}

	}

}
