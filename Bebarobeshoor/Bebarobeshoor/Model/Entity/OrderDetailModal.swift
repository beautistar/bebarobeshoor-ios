//
//	OrderDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OrderDetailModal : NSObject, NSCoding {

	var copon : String!
	var createDate : String!
    
	var dropoffDate : String!
	var end : String!
	var idCarieroff : String!
	var idCarierup : String!
	var idCustomer : String!
	var idDropoff : String!
	var idOrder : String!
	var idPakage : String!
	var idPickup : String!
	var pickupDate : String!
	var price : String!
	var shows : String!
	var state : String!
	var typePay : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		copon = dictionary["copon"] as? String
		createDate = dictionary["create_date"] as? String
		dropoffDate = dictionary["dropoff_date"] as? String
		end = dictionary["end"] as? String
		idCarieroff = dictionary["id_carieroff"] as? String
		idCarierup = dictionary["id_carierup"] as? String
		idCustomer = dictionary["id_customer"] as? String
		idDropoff = dictionary["id_dropoff"] as? String
		idOrder = dictionary["id_order"] as? String
		idPakage = dictionary["id_pakage"] as? String
		idPickup = dictionary["id_pickup"] as? String
		pickupDate = dictionary["pickup_date"] as? String
		price = dictionary["price"] as? String
		shows = dictionary["shows"] as? String
		state = dictionary["state"] as? String
		typePay = dictionary["type_pay"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if copon != nil{
			dictionary["copon"] = copon
		}
		if createDate != nil{
			dictionary["create_date"] = createDate
		}
		if dropoffDate != nil{
			dictionary["dropoff_date"] = dropoffDate
		}
		if end != nil{
			dictionary["end"] = end
		}
		if idCarieroff != nil{
			dictionary["id_carieroff"] = idCarieroff
		}
		if idCarierup != nil{
			dictionary["id_carierup"] = idCarierup
		}
		if idCustomer != nil{
			dictionary["id_customer"] = idCustomer
		}
		if idDropoff != nil{
			dictionary["id_dropoff"] = idDropoff
		}
		if idOrder != nil{
			dictionary["id_order"] = idOrder
		}
		if idPakage != nil{
			dictionary["id_pakage"] = idPakage
		}
		if idPickup != nil{
			dictionary["id_pickup"] = idPickup
		}
		if pickupDate != nil{
			dictionary["pickup_date"] = pickupDate
		}
		if price != nil{
			dictionary["price"] = price
		}
		if shows != nil{
			dictionary["shows"] = shows
		}
		if state != nil{
			dictionary["state"] = state
		}
		if typePay != nil{
			dictionary["type_pay"] = typePay
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         copon = aDecoder.decodeObject(forKey: "copon") as? String
         createDate = aDecoder.decodeObject(forKey: "create_date") as? String
         dropoffDate = aDecoder.decodeObject(forKey: "dropoff_date") as? String
         end = aDecoder.decodeObject(forKey: "end") as? String
         idCarieroff = aDecoder.decodeObject(forKey: "id_carieroff") as? String
         idCarierup = aDecoder.decodeObject(forKey: "id_carierup") as? String
         idCustomer = aDecoder.decodeObject(forKey: "id_customer") as? String
         idDropoff = aDecoder.decodeObject(forKey: "id_dropoff") as? String
         idOrder = aDecoder.decodeObject(forKey: "id_order") as? String
         idPakage = aDecoder.decodeObject(forKey: "id_pakage") as? String
         idPickup = aDecoder.decodeObject(forKey: "id_pickup") as? String
         pickupDate = aDecoder.decodeObject(forKey: "pickup_date") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         shows = aDecoder.decodeObject(forKey: "shows") as? String
         state = aDecoder.decodeObject(forKey: "state") as? String
         typePay = aDecoder.decodeObject(forKey: "type_pay") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if copon != nil{
			aCoder.encode(copon, forKey: "copon")
		}
		if createDate != nil{
			aCoder.encode(createDate, forKey: "create_date")
		}
		if dropoffDate != nil{
			aCoder.encode(dropoffDate, forKey: "dropoff_date")
		}
		if end != nil{
			aCoder.encode(end, forKey: "end")
		}
		if idCarieroff != nil{
			aCoder.encode(idCarieroff, forKey: "id_carieroff")
		}
		if idCarierup != nil{
			aCoder.encode(idCarierup, forKey: "id_carierup")
		}
		if idCustomer != nil{
			aCoder.encode(idCustomer, forKey: "id_customer")
		}
		if idDropoff != nil{
			aCoder.encode(idDropoff, forKey: "id_dropoff")
		}
		if idOrder != nil{
			aCoder.encode(idOrder, forKey: "id_order")
		}
		if idPakage != nil{
			aCoder.encode(idPakage, forKey: "id_pakage")
		}
		if idPickup != nil{
			aCoder.encode(idPickup, forKey: "id_pickup")
		}
		if pickupDate != nil{
			aCoder.encode(pickupDate, forKey: "pickup_date")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if shows != nil{
			aCoder.encode(shows, forKey: "shows")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if typePay != nil{
			aCoder.encode(typePay, forKey: "type_pay")
		}
	}
}
