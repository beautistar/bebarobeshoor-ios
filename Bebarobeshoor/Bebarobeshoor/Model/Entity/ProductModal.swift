//
//	ProductModal.swift
//
//	Create by Dharmesh Avaiya on 18/2/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ProductModal : NSObject, NSCoding {

	var category : String!
	var descriptionField : String!
	var idProduct : String!
	var image : String!
	var price : String!
	var title : String!
    var tickCount : Double!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		category = dictionary["category"] as? String
		descriptionField = dictionary["description"] as? String
		idProduct = dictionary["id_product"] as? String
		image = dictionary["image"] as? String
		price = dictionary["price"] as? String
		title = dictionary["title"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if category != nil{
			dictionary["category"] = category
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if idProduct != nil{
			dictionary["id_product"] = idProduct
		}
		if image != nil{
			dictionary["image"] = image
		}
		if price != nil{
			dictionary["price"] = price
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         category = aDecoder.decodeObject(forKey: "category") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         idProduct = aDecoder.decodeObject(forKey: "id_product") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {
        
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if idProduct != nil{
			aCoder.encode(idProduct, forKey: "id_product")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}
}

