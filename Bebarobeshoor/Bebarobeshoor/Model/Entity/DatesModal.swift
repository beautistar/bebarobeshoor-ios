//
//	DatesModal.swift
//
//	Create by Dharmesh Avaiya on 19/2/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftDate

let kReponseDateFormat = "yyyy-MM-dd"

class DatesModal : NSObject, NSCoding{

	var date : String!
    var day : String!
	var success : Int!
    
     // New
    var monthInDate : String! = nil
    var dayInWeek : String! = nil
    var dayInDate : String! = nil
    var formatedDate : String! = nil  //MMM - dd - EEE
    
    
 

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary) {
        
        date = dictionary["Date"] as? String
		success = dictionary["Success"] as? Int
        day = try! date.date(format: DateFormat.custom(kReponseDateFormat)).string(custom: "EEE")
        
        var dteTo : String!
        dteTo = dictionary["Date"] as? String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date1 = dateFormatter.date(from: dteTo)!
            dateFormatter.dateFormat = "MMM"
            dateFormatter.calendar = NSCalendar(identifier: NSCalendar.Identifier.persian)! as Calendar
            dateFormatter.locale = NSLocale(localeIdentifier: "fa_IR") as Locale!
            monthInDate = dateFormatter.string(from: date1)
            
        
        
        var dateDay : String!
        dateDay = dictionary["Date"] as? String
        let dateFormatterDay = DateFormatter()
        dateFormatterDay.dateFormat = "yyyy-MM-dd"
        let date2 = dateFormatterDay.date(from: dateDay)!
            dateFormatterDay.dateFormat = "EEE"
            dateFormatterDay.calendar = NSCalendar(identifier: NSCalendar.Identifier.persian)! as Calendar
            dateFormatterDay.locale = NSLocale(localeIdentifier: "fa_IR") as Locale!
            dayInWeek = dateFormatterDay.string(from: date2)
      
        
        var dateDayN : String!
        dateDayN = dictionary["Date"] as? String
        let dateFormatterDayN = DateFormatter()
        dateFormatterDayN.dateFormat = "yyyy-MM-dd"
        let date3 = dateFormatterDayN.date(from: dateDayN)!
        dateFormatterDayN.dateFormat = "dd"
        dayInDate = dateFormatterDayN.string(from: date3)
        
        formatedDate = "\(monthInDate ?? "") \(dayInDate ?? "") \(dayInWeek ?? "")"
        print("formatedDate", formatedDate! as String)
    }
    

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
        
		if date != nil{
			dictionary["Date"] = date
		}
		if success != nil{
			dictionary["Success"] = success
		}
        if day != nil{
            dictionary["day"] = day
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         date = aDecoder.decodeObject(forKey: "Date") as? String
         success = aDecoder.decodeObject(forKey: "Success") as? Int
        day = aDecoder.decodeObject(forKey: "day") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if date != nil{
			aCoder.encode(date, forKey: "Date")
		}
        
		if success != nil{
			aCoder.encode(success, forKey: "Success")
		}
        
        if day != nil{
            aCoder.encode(day, forKey: "day")
        }

	}

}
