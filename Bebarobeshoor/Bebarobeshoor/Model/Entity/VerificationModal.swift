//
//	Response.swift
//
//	Create by Dharmesh Avaiya on 14/2/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class VerificationModal : NSObject, NSCoding{

	var oTP : Int!
	var code : String!
	var idCustomers : Int!
	var msg : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		oTP = dictionary["OTP"] as? Int
		code = dictionary["code"] as? String
		idCustomers = dictionary["id_customers"] as? Int
		msg = dictionary["msg"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let
        dictionary = NSMutableDictionary()
		if oTP != nil{
			dictionary["OTP"] = oTP
		}
		if code != nil{
			dictionary["code"] = code
		}
		if idCustomers != nil{
			dictionary["id_customers"] = idCustomers
		}
		if msg != nil{
			dictionary["msg"] = msg
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         oTP = aDecoder.decodeObject(forKey: "OTP") as? Int
         code = aDecoder.decodeObject(forKey: "code") as? String
         idCustomers = aDecoder.decodeObject(forKey: "id_customers") as? Int
         msg = aDecoder.decodeObject(forKey: "msg") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if oTP != nil{
			aCoder.encode(oTP, forKey: "OTP")
		}
		if code != nil{
			aCoder.encode(code, forKey: "code")
		}
		if idCustomers != nil{
			aCoder.encode(idCustomers, forKey: "id_customers")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
	}
}
