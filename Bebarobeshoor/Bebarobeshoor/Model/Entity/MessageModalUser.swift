//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class MessageModalUser : NSObject, NSCoding {
    
    var code : String!
    var customerData : UserModal!
    var msg : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        code = dictionary["code"] as? String
        if let customerDataData = dictionary["customer_data"] as? NSDictionary{
            customerData = UserModal(fromDictionary: customerDataData)
        }
        msg = dictionary["msg"] as? String
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if code != nil{
            dictionary["code"] = code
        }
        if customerData != nil{
            dictionary["customer_data"] = customerData.toDictionary()
        }
        if msg != nil{
            dictionary["msg"] = msg
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        code = aDecoder.decodeObject(forKey: "code") as? String
        customerData = aDecoder.decodeObject(forKey: "customer_data") as? UserModal
        msg = aDecoder.decodeObject(forKey: "msg") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder) {

        if code != nil{
            aCoder.encode(code, forKey: "code")
        }
        if customerData != nil{
            aCoder.encode(customerData, forKey: "customer_data")
        }
        if msg != nil{
            aCoder.encode(msg, forKey: "msg")
        }
    }
}
