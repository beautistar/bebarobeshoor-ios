//
//	Response.swift
//
//	Create by Dharmesh Avaiya on 19/2/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PackageModal : NSObject, NSCoding{

	var icon : String!
	var idPackage : String!
	var name : String!
	var orderFee : String!
	var orderFee1 : String!
	var serviceFee : String!
	var timeSlot : String!
	var timeTurnaround : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		icon = dictionary["icon"] as? String
		idPackage = dictionary["id_package"] as? String
		name = dictionary["name"] as? String
		orderFee = dictionary["order_fee"] as? String
		orderFee1 = dictionary["order_fee1"] as? String
		serviceFee = dictionary["service_fee"] as? String
		timeSlot = dictionary["time_slot"] as? String
		timeTurnaround = dictionary["time_turnaround"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
        
		if icon != nil{
			dictionary["icon"] = icon
		}
		if idPackage != nil{
			dictionary["id_package"] = idPackage
		}
		if name != nil{
			dictionary["name"] = name
		}
		if orderFee != nil{
			dictionary["order_fee"] = orderFee
		}
		if orderFee1 != nil{
			dictionary["order_fee1"] = orderFee1
		}
		if serviceFee != nil{
			dictionary["service_fee"] = serviceFee
		}
		if timeSlot != nil{
			dictionary["time_slot"] = timeSlot
		}
		if timeTurnaround != nil{
			dictionary["time_turnaround"] = timeTurnaround
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         icon = aDecoder.decodeObject(forKey: "icon") as? String
         idPackage = aDecoder.decodeObject(forKey: "id_package") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         orderFee = aDecoder.decodeObject(forKey: "order_fee") as? String
         orderFee1 = aDecoder.decodeObject(forKey: "order_fee1") as? String
         serviceFee = aDecoder.decodeObject(forKey: "service_fee") as? String
         timeSlot = aDecoder.decodeObject(forKey: "time_slot") as? String
         timeTurnaround = aDecoder.decodeObject(forKey: "time_turnaround") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if icon != nil{
			aCoder.encode(icon, forKey: "icon")
		}
		if idPackage != nil{
			aCoder.encode(idPackage, forKey: "id_package")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if orderFee != nil{
			aCoder.encode(orderFee, forKey: "order_fee")
		}
		if orderFee1 != nil{
			aCoder.encode(orderFee1, forKey: "order_fee1")
		}
		if serviceFee != nil{
			aCoder.encode(serviceFee, forKey: "service_fee")
		}
		if timeSlot != nil{
			aCoder.encode(timeSlot, forKey: "time_slot")
		}
		if timeTurnaround != nil{
			aCoder.encode(timeTurnaround, forKey: "time_turnaround")
		}
	}
}

