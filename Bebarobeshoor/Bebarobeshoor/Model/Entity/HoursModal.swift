//
//	Hour.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HoursModal : NSObject, NSCoding {

	var idPickup : String!
	var name : String!
	var time : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		idPickup = dictionary["id_pickup"] as? String
		name = dictionary["name"] as? String
		time = dictionary["time"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if idPickup != nil{
			dictionary["id_pickup"] = idPickup
		}
		if name != nil{
			dictionary["name"] = name
		}
		if time != nil{
			dictionary["time"] = time
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         idPickup = aDecoder.decodeObject(forKey: "id_pickup") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         time = aDecoder.decodeObject(forKey: "time") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if idPickup != nil{
			aCoder.encode(idPickup, forKey: "id_pickup")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if time != nil{
			aCoder.encode(time, forKey: "time")
		}
	}
}
