//
//  RequestParamModal.swift
//  Bebarobeshoor
//
//  Created by Developer on 13/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation
import UIKit

class RequestParamModal : NSObject {
    
    var userId : String!
    var userUndId : String!
    var firstName : String!
    var lastName : String!
    var email : String!
    var phoneNumber : String!
    var password : String!
    var pass : String!
    var confirmPassword : String!
    var area : String!
    var address : String!
    var flatNumber : String!
    var houseNumber : String!
    var postalCode : String!
    var floorNumber : String!
    var referralcode : String!
    var addamount : String!
    var discount : String!
    var descript : String!
    
    //Order
    var idPackage : String!
    var idPickup : String!
    var idDropOff : String!
    var price : String!
    var idCustomer : String!
    var state : String!
    var createDate : String!
    var pickupDate : String!
    var dropOffDate : String!
    var idCarrierUp : String!
    var idCarrierOff : String!
    var end : String!
    var shows : String!
    var copon : String!
    var typePay : String!
    var products : String!
    
    //profile
    var customerId : String!
    var mobile : String!
    var latitude : String!
    var longitude : String!
    
    //cancel order
    var orderId : String!
    
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if userId != nil{
            dictionary["userid"] = userId
        }
        if userUndId != nil{
            dictionary["user_id"] = userUndId
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if email != nil{
            dictionary["email"] = email
        }
        if phoneNumber != nil{
            dictionary["phone_number"] = phoneNumber
        }
        if password != nil{
            dictionary["password"] = password
        }
        
        if pass != nil{
            dictionary["pass"] = password
        }
        
        if confirmPassword != nil{
            dictionary["confirm_password"] = confirmPassword
        }
        if area != nil{
            dictionary["area"] = area
        }
        if address != nil{
            dictionary["address"] = address
        }
        if flatNumber != nil{
            dictionary["flat_number"] = flatNumber
        }
        if houseNumber != nil{
            dictionary["house_number"] = houseNumber
        }
        if postalCode != nil{
            dictionary["postal_code"] = postalCode
        }
        if floorNumber != nil{
            dictionary["floor_number"] = floorNumber
        }
        
        if referralcode != nil{
            dictionary["referralcode"] = referralcode
        }
        
        if addamount != nil{
            dictionary["addamount"] = addamount
        }
        
        if idPackage != nil{
            dictionary["id_pakage"] = idPackage
        }
        if idPickup != nil{
            dictionary["id_pickup"] = idPickup
        }
        if idDropOff != nil{
            dictionary["id_dropoff"] = idDropOff
        }
        if price != nil{
            dictionary["price"] = price
        }
        if idCustomer != nil{
            dictionary["id_customer"] = idCustomer
        }
        if state != nil{
            dictionary["state"] = state
        }
        if createDate != nil{
            dictionary["create_date"] = createDate
        }
        if pickupDate != nil{
            dictionary["pickup_date"] = pickupDate
        }
        if dropOffDate != nil{
            dictionary["dropoff_date"] = dropOffDate
        }
        if idCarrierUp != nil{
            dictionary["id_carierup"] = idCarrierUp
        }
        if idCarrierOff != nil{
            dictionary["id_carieroff"] = idCarrierOff
        }
        if end != nil{
            dictionary["end"] = end
        }
        if shows != nil{
            dictionary["shows"] = shows
        }
        if copon != nil{
            dictionary["copon"] = copon
        }
        if typePay != nil{
            dictionary["type_pay"] = typePay
        }
        if customerId != nil {
            dictionary["customer_id"] = customerId
        }
        if mobile != nil {
            dictionary["mobile"] = mobile
        }
        if orderId != nil {
            dictionary["order_id"] = orderId
        }
        
        if products != nil {
            dictionary["products"] = products
        }
        
        if latitude != nil {
            dictionary["lat"] = latitude
        }
        
        if longitude != nil {
            dictionary["lon"] = longitude
        }
        if descript != nil{
             dictionary["description"] = descript
        }
        
        return dictionary
    }
}
