//
//	Result.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PayAuthResult : NSObject, NSCoding{

	var data : PayAuthData!
	var message : String!
	var statusCode : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let dataData = dictionary["data"] as? NSDictionary{
			data = PayAuthData(fromDictionary: dataData)
		}
		message = dictionary["message"] as? String
		statusCode = dictionary["statusCode"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
    
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		
        if data != nil{
			dictionary["data"] = data.toDictionary()
		}
		if message != nil{
			dictionary["message"] = message
		}
		if statusCode != nil{
			dictionary["statusCode"] = statusCode
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         data = aDecoder.decodeObject(forKey: "data") as? PayAuthData
         message = aDecoder.decodeObject(forKey: "message") as? String
         statusCode = aDecoder.decodeObject(forKey: "statusCode") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if statusCode != nil{
			aCoder.encode(statusCode, forKey: "statusCode")
		}

	}

}
