//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class MessageModalHours : NSObject, NSCoding{

	var code : String!
	var hours : [HoursModal]!
	var msg : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		code = dictionary["code"] as? String
		hours = [HoursModal]()
		if let hoursArray = dictionary["hours"] as? [NSDictionary]{
			for dic in hoursArray{
				let value = HoursModal(fromDictionary: dic)
				hours.append(value)
			}
		}
		msg = dictionary["msg"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if code != nil{
			dictionary["code"] = code
		}
		if hours != nil{
			var dictionaryElements = [NSDictionary]()
			for hoursElement in hours {
				dictionaryElements.append(hoursElement.toDictionary())
			}
			dictionary["hours"] = dictionaryElements
		}
		if msg != nil{
			dictionary["msg"] = msg
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         code = aDecoder.decodeObject(forKey: "code") as? String
         hours = aDecoder.decodeObject(forKey: "hours") as? [HoursModal]
         msg = aDecoder.decodeObject(forKey: "msg") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if code != nil{
			aCoder.encode(code, forKey: "code")
		}
		if hours != nil{
			aCoder.encode(hours, forKey: "hours")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
	}
}
