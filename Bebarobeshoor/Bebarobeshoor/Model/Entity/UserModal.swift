//
//  UserModal.swift
//  Bebarobeshoor
//
//  Created by Developer on 12/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation
import UIKit

class UserModal  : NSObject, NSCoding {
    
    var userid : String!
    var email : String!
    var firstname : String!
    var lastname : String!
    var mobile : String!
    var discount : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary) {
        
        userid = dictionary["userid"] as? String
        email = dictionary["email"] as? String
        firstname = dictionary["firstname"] as? String
        lastname = dictionary["lastname"] as? String
        mobile = dictionary["mobile"] as? String
        discount = dictionary["discount"] as? String
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if userid != nil{
            dictionary["userid"] = userid
        }
        if email != nil{
            dictionary["email"] = email
        }
        if firstname != nil{
            dictionary["firstname"] = firstname
        }
        if lastname != nil{
            dictionary["lastname"] = lastname
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if discount != nil {
            dictionary["discount"] = discount
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder) {
        
        userid = aDecoder.decodeObject(forKey: "userid") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        firstname = aDecoder.decodeObject(forKey: "firstname") as? String
        lastname = aDecoder.decodeObject(forKey: "lastname") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        discount = aDecoder.decodeObject(forKey: "discount") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder) {

        if userid != nil{
            aCoder.encode(userid, forKey: "userid")
        }
        if email != nil {
            aCoder.encode(email, forKey: "email")
        }
        if firstname != nil {
            aCoder.encode(firstname, forKey: "firstname")
        }
        if lastname != nil {
            aCoder.encode(lastname, forKey: "lastname")
        }
        if mobile != nil {
            aCoder.encode(mobile, forKey: "mobile")
        }
        if discount != nil {
            aCoder.encode(discount, forKey: "discount")
        }
    }
}
