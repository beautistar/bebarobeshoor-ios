//
//  NSObject+Extension.swift
//  Bebarobeshoor
//
//  Created by Developer on 1/31/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation
import UIKit

extension NSObject {
    
    class var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last! as String
    }
    
    //    class var identifier: String {
    //        return String(format: "%@_identifier", self.nameOfClass)
    //    }
}
