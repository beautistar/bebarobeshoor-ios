//
//  NewPwdViewController.swift
//  Bebarobeshoor
//
//  Created by Tejas Dattani on 4/12/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class NewPwdViewController: BaseNavigationVC {
    
    
 
    var strPhoneNumber = String()
    var strCode = String()
    
    @IBOutlet weak var txtCode: BEBUITextField!
    @IBOutlet weak var txtNewPassword: BEBUITextField!
    @IBOutlet weak var btnChangePassword: BEBUIButton!
    
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func IsValidAllInputs() -> Bool {
        if txtCode.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtCode)
            return false
        }
        
        if txtNewPassword.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtNewPassword)
            return false
        }
        
        if txtNewPassword.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtNewPassword)
            return false
        }
        
        if strCode != txtCode.text {
            showValidFieldMessage(textField: txtCode)
            return false
        }
        
        return true
    }
    
    
    //------------------------------------------------------
    //MARK: Action Methods
    //------------------------------------------------------
    
    @IBAction func backAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func IBBtnChangePassword(_ sender: Any) {
        
        if IsValidAllInputs() == false {
            return
        }
        
        let parameters = RequestParamModal()
        
        parameters.phoneNumber = strPhoneNumber
        parameters.pass = txtNewPassword.text
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.Verification) as! VerificationViewController
        controller.requestParameter = parameters
        
        self.view.endEditing(true)
        
        RequestManager.singleton.requestGET(requestMethod: kAPIForgotPassword, parameters: parameters.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            if let message = response.object(forKey: "msg") {
                messageBox(message: String(describing: message))
            }
            _ = self.navigationController?.popToRootViewController(animated: true)

            
        }) { (response : NSDictionary?, error : Error?) in

            if let message = response?.object(forKey: "msg") {
                messageBox(message: String(describing: message))
            }
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        btnChangePassword.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        btnChangePassword.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnChangePassword.layer.shadowOpacity = 1.0
        btnChangePassword.layer.shadowRadius = 10.0
        btnChangePassword.layer.masksToBounds = false
        btnChangePassword.layer.cornerRadius = 5.0
        // Do any additional setup after loading the view.
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
