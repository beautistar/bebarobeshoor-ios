//
//  NSString+NSHash.h
//  RadPos
//
//  Created by Amir Soleimani on 3/8/16.
//  Copyright © 2016 Amir Soleimani. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSString (NSHash_AdditionalHashingAlgorithms)

/**
 Creates a MD5 hash of the current string as hex NSString representation.
 */
- (nonnull NSString*) MD5;

/**
 Creates a MD5 hash of the current string as NSData representation.
 */
- (nonnull NSData*) MD5Data;

/**
 Creates a SHA1 hash of the current string as hex NSString representation.
 */
- (nonnull NSString*) SHA1;

/**
 Creates a SHA1 hash of the current string as NSData representation.
 */
- (nonnull NSData*) SHA1Data;

/**
 Creates a SHA256 hash of the current string as hex NSString representation.
 */
- (nonnull NSString*) SHA256;

/**
 Creates a SHA256 hash of the current string as NSData representation.
 */
- (nonnull NSData*) SHA256Data;

@end