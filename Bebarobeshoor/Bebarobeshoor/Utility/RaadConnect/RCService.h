//
//  APIService.h
//  RaadConnect - Parsian
//
//  Created by Amir Soleimani (amir.so@icloud.com) on 7/24/16.
//
//  Copyright © 2016 Amir Soleimani. All rights reserved.
//


@import SafariServices;
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "RCNSString+NSHash.h"
#import "RCRSAEncryption.h"
#import "RCReachability.h"


@protocol RCServiceDelegate <NSObject>

@optional
- (void)RCStartLoading;
- (void)RCStopLoading;

- (void)RCServiceError:(NSString*)Error;

- (void)RCCallBackSuccessPayAuth:(NSString*)PA;
- (void)RCCallBackFailedPayAuth:(NSString*)Error;

- (void)RCCallBackFailedPaymentVerify:(NSString *)Error;
- (void)RCCallBackSuccessPaymentVerify:(NSDictionary *)Response;

- (void)RCCallBackFailedGetUserToken:(NSString*)Error;
- (void)RCCallBackSuccessGetUserToken:(NSString*)Token;
@end

@interface RCService : NSObject <NSURLSessionDelegate,SFSafariViewControllerDelegate>

@property(nonatomic, weak) id delegate;
@property(nonatomic, assign) BOOL TestMode;


/*
 * Methods
 */
- (NSString*)GetUserToken;
- (void)GeneratePayAuth:(long)Amount orderid:(long)OrderId callbackurl:(NSString*)CallBackURL apikey:(NSString *)AK;
- (void)OpenWebPayment:(NSString*)PayAuth apikey:(NSString *)AK controller:(UIViewController *)myself token:(NSString*)Token;
- (void)OpenWebPayment:(NSString*)PayAuth apikey:(NSString *)AK controller:(UIViewController *)myself;
- (void)CallBackPaymentVerify:(NSURL*)url payauth:(NSString*)PayAuth apikey:(NSString*)AK;
- (void)RCPaymentVerify:(NSString*)Token payauth:(NSString*)PayAuth apikey:(NSString*)AK;
- (void)RCGetUserToken:(NSString*)PayAuth apikey:(NSString*)AK;

@end
