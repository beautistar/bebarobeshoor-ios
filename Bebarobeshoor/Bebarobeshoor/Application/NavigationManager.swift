//
//  NavigationManager.swift
//  Bebarobeshoor
//
//  Created by Developer on 11/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift

enum NavigationScreenType {
    
    case Home
    case AddContact
    case Verification
    case PriceCalculator
    case CardInvoice
    case BankInvoice
    case MyProfile
    case Order
    case Help
    case HowIt
    case Legal
    case Payment
    case OrderSummary
    case OrderStatus
    case NewPassword
}

enum NavigationType {
    
    case NavigationTypeHome
    case NavigationTypeLogin
}

class NavigationManager : NSObject {
    
    static let singleton : NavigationManager = NavigationManager()
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let window = AppDelegate.singleton.window
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Setup Roots
    
    func setupLogin() {
        
        let loginVC = mainStoryboard.instantiateViewController(withIdentifier: "IntroNav") as! UINavigationController
        window?.backgroundColor = kColorBackground
        window?.tintColor = kColorNavBar
        window?.rootViewController = loginVC
        window?.makeKeyAndVisible()
    }
    
    func setupHome() {
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = self.screen(screenType: NavigationScreenType.MyProfile) as! MyProfileTableViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = kColorNavBar
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        nvc.title = "New Order"
        
        UINavigationBar.appearance().barTintColor = kColorNavBar
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(named:"BgVav.png"),
                                                        for: .default)
        
        rightViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        window?.tintColor = kColorNavBar
        self.window?.backgroundColor = kColorBackground
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = kColorNavBar
        self.window?.addSubview(statusBarView)
         UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    
    
    func navigation(screenType : NavigationType) -> UINavigationController {
                      
        switch screenType {
            
            case .NavigationTypeHome:
                let controller = mainStoryboard.instantiateViewController(withIdentifier: "NavigationHomeVC") as! UINavigationController
             
                return controller
            
            default:
                break
        }
       
        
        return UINavigationController()
    }
    
    
    
    func screen(screenType : NavigationScreenType) -> UIViewController {
        
        switch screenType {
            
        case .Home :
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(MainViewController.self)) as! MainViewController
            return controller
            
            case .AddContact :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(AddContactViewController.self)) as! AddContactViewController
                return controller
            
            case .Verification :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(VerificationViewController.self)) as! VerificationViewController
                return controller
            
            case .PriceCalculator :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(PricingViewController.self)) as! PricingViewController
                return controller
            case .BankInvoice :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(BankInvoiceViewController.self)) as! BankInvoiceViewController
                return controller
            case .CardInvoice :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(CardInvoiceViewController.self)) as! CardInvoiceViewController
                return controller
            case .MyProfile :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(MyProfileTableViewController.self)) as! MyProfileTableViewController
                return controller
            case .Order :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(OrderViewController.self)) as! OrderViewController
                return controller
            case .Help :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(HelpViewController.self)) as! HelpViewController
                return controller
            case .HowIt :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(HowItViewController.self)) as! HowItViewController
                return controller
            case .Legal :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(LegalViewController.self)) as! LegalViewController
                return controller
            case .Payment :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(PaymentViewController.self)) as! PaymentViewController
                return controller
            case .OrderSummary :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(SummaryViewController.self)) as! SummaryViewController
                    return controller
            case .OrderStatus :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(OrderStatusViewController.self)) as! OrderStatusViewController
                return controller
            case .NewPassword :
                let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(NewPwdViewController.self)) as! NewPwdViewController
                return controller
        }
        
        return UIViewController()
    }
    
    //------------------------------------------------------
    
    func shareIt(view : UIViewController, message : String?) {
        
        //Set the default sharing message.
        let message = message ?? kAppName.localized()
        
        //Set the link to share.
        if let link = NSURL(string: "https://us.letgo.com")
        {
            let objectsToShare = [message, link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            view.present(activityVC, animated: true, completion: nil)
        }
    }
    
    
        
    //------------------------------------------------------
}

extension UINavigationController
{
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
}
var preferredStatusBarStyle: UIStatusBarStyle {
    get { return .lightContent }
}
