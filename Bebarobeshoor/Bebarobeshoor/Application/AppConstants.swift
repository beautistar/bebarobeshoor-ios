//
//  AppConstants.swift
//  Bebarobeshoor
//
//  Created by Developer on 11/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Foundation
import MBProgressHUD
import Localize_Swift

let kAppName = "Bebarobeshoor".localized()

let kColorBackground = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
let kColorGreen = UIColor.init(hex: "3DE349")
let kColorNavBar = UIColor.init(hex: "5AE45B")


var MyNotificationCenter : NotificationCenter {
    return NotificationCenter.default
}

var BlankView : UIView {
    
    let blankView = UIView()
    blankView.backgroundColor = UIColor.clear
    return blankView
}

var ScreenRect : CGRect {
    return UIScreen.main.bounds
}

var MyUserDefaults : UserDefaults {
    return UserDefaults.standard
}

let kCurrentUser = "currentUser"
let kCurrentUserId = "currentUserId"

var currentUser : UserModal? {
    
    if let userData = MyUserDefaults.object(forKey: kCurrentUser) as? NSData {
        let dict = NSKeyedUnarchiver.unarchiveObject(with: userData as Data) as! NSDictionary
        return UserModal(fromDictionary: dict)
    }
    return nil
}

let kCancel = "Cancel".localized()
let kOk = "Ok".localized()
let kCamera = "Camera".localized()
let kGallery = "Gallery".localized()
let kSettings = "Settings".localized()

func LocalizedControl(view : Any) {
    
    if view is UIButton {
        
        let button = view as! UIButton
        let keyNormal = button.title(for: .normal)
        let keyHighlighted = button.title(for: .highlighted)
        let keySelected = button.title(for: .selected)
        let keyDisable = button.title(for: .disabled)
        
        if keyNormal != nil {
            button .setTitle(keyNormal!.localized(), for: .normal)
        }
        
        if keyHighlighted != nil {
            button .setTitle(keyNormal!.localized(), for: .highlighted)
        }
        
        if keySelected != nil {
            button .setTitle(keyNormal!.localized(), for: .selected)
        }
        
        if keyDisable != nil {
            button .setTitle(keyNormal!.localized(), for: .disabled)
        }
    }
    
    if view is UILabel {
        
        let label = view as! UILabel
        let key = label.text
        
        if key != nil {
            label.text = key!.localized()
        }
    }
    
    if view is UITextField {
        
        let label = view as! UITextField
        let key = label.placeholder
        
        if key != nil {
            label.placeholder = key!.localized()
        }
    }
}

func LocalizedControl(view : Any, key : String) {
    
    if view is UIButton {
        
        let button = view as! UIButton
        
        button .setTitle(key.localized(), for: .normal)
        button .setTitle(key.localized(), for: .highlighted)
        button .setTitle(key.localized(), for: .selected)
        button .setTitle(key.localized(), for: .disabled)
    }
    
    if view is UILabel {
        
        let label = view as! UILabel
        label.text = key.localized()
    }
    
    if view is UITextField {
        
        let label = view as! UITextField
        label.placeholder = key.localized()
    }
}

func messageBox(message : String!) {
    let av = UIAlertView(title: kAppName, message: message.localized(), delegate: nil, cancelButtonTitle: kOk)
    av.show()
}

func showEmptyFieldMessage(textField : UITextField) {
    messageBox(message: "لطفا وارد \(textField.placeholder!)")
}

func showValidFieldMessage(textField : UITextField) {
    messageBox(message: "لطفا معتبر وارد کنید \(textField.placeholder!.lowercased())")
}

//Date and Time
let kNewOrderDateFormate = "d.MMMM"
let kNewOrderTimeFormate = "HH:mm"

//let GoogleMapKey = "AIzaSyDXmlZ1Lb_NNJH1D4pT1Sz5D-v0W61D9zw"
let GoogleMapKey = "AIzaSyCV2Cic_kisg9FSuHhcd4X4ql4JhfSuKfM"
let kPaymentAPIKey = "9A81F84B-4140-4C62-B1FE-7FB7D87F05ED"

