//
//  BankInvoiceViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/5/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class BankInvoiceViewController: UIViewController {

    @IBOutlet weak var lblInvoiveNumber: BEBUILable!
    @IBOutlet weak var infoView: UIView!
    var invoiceNumber : String!
    @IBOutlet weak var continueB: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblInvoiveNumber.text = invoiceNumber ?? ""
        lblInvoiveNumber.layer.cornerRadius = 15
        lblInvoiveNumber.layer.masksToBounds = true

        continueB.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        continueB.layer.shadowOffset = CGSize(width: 0, height: 3)
        continueB.layer.shadowOpacity = 1.0
        continueB.layer.shadowRadius = 5.0
        continueB.layer.masksToBounds = false
        continueB.layer.cornerRadius = 4.0
        
      //  infoView.backgroundColor = UIColor.green
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneTap(_ sender: BEBUIButton) {
     
        _ = navigationController?.popToRootViewController(animated: true)
    }
}
