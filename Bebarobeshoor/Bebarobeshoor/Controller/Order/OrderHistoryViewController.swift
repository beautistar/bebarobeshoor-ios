//
//  OrderHistoryViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/7/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

import UIKit
import Foundation

class OrderHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblOrderHistory: UITableView!
    
    var orders : [OrderDetailModal] = []
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func requestGetOrders() {
        
        let parameters = RequestParamModal()
        parameters.userUndId = currentUser?.userid
        
        RequestManager.singleton.requestGET(requestMethod: kAPIOrderStatus, parameters: parameters.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            let messageModal = MessageOrderModal(fromDictionary: response)
            self.orders = messageModal.orderDetails
            self.tblOrderHistory.reloadData()
            
        }) { (response : NSDictionary?, error : Error?) in
            
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    //------------------------------------------------------
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: OrderHistoryCell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryCell", for: indexPath) as! OrderHistoryCell
        let order = orders[indexPath.row]
        cell.lblNumber.text = "Invoice Number".localized().appending(". ").appending(order.idOrder)
        cell.lblDate.text = order.dropoffDate
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let order = orders[indexPath.row]
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.OrderStatus) as! OrderStatusViewController
        controller.orderDetail = order
        controller.orders = orders
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        requestGetOrders()
    }
}
