//
//  PaymentViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/5/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController, RCServiceDelegate {

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var imvBankCheck: UIImageView!
    @IBOutlet weak var imvCardCheck: UIImageView!
    @IBOutlet weak var lblTotalCost: BEBUILable!
    @IBOutlet weak var payByCash: UIButton!
    @IBOutlet weak var PayByCard: UIButton!
    @IBOutlet weak var continueB: UIButton!
    
    var requestParameters : RequestParamModal?    
    var orderId : Int?
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    //------------------------------------------------------
    
    @IBAction func bankPayAction(_ sender: Any) {
        
        imvBankCheck.image = UIImage.init(named: "ic_radio_checked")
        imvCardCheck.image = UIImage.init(named: "ic_radio_unchecked")
        requestParameters?.typePay = "0"
        payByCash.backgroundColor = UIColor.white
        PayByCard.backgroundColor = UIColor.init(hex:"15C916")
    }
    
    
    @IBAction func cardPayAction(_ sender: Any) {
        
        imvBankCheck.image = UIImage.init(named: "ic_radio_unchecked")
        imvCardCheck.image = UIImage.init(named: "ic_radio_checked")
        requestParameters?.typePay = "1"
        payByCash.backgroundColor = UIColor.init(hex:"15C916")
        PayByCard.backgroundColor = UIColor.white
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCountinueTap(_ sender: BEBUIButton) {
        
        //FIXME: make a dynamic
        requestParameters?.idCarrierUp = "100"
        requestParameters?.idCarrierOff = "100"
        requestParameters?.end = "0"
        requestParameters?.shows = "1"
        requestParameters?.shows = "1"
        requestParameters?.idPickup = "0"
        requestParameters?.idDropOff = "0"
        requestParameters?.state = "4"
        
        if requestParameters?.copon.characters.count == 0 {
            requestParameters?.copon = "0"
        }
        
        var products : Array<Dictionary<String, String>> = Array()
        
        for iProduct in ProductManager.singleton.products {
            
            products.append(["prouctid" : iProduct.idProduct,
                            "quantity" : String(Int(iProduct.tickCount))])
        }
        
        let data = try! JSONSerialization.data(withJSONObject: products, options: JSONSerialization.WritingOptions.prettyPrinted)
        requestParameters?.products = String(data: data, encoding: String.Encoding.utf8)
        
        //products=[{%22prouctid%22=%2212%22,%22quantity%22=2},{%22prouctid%22=%22122%22,%22quantity%22=1},{%22prouctid%22=%223%22,%22quantity%22=5}]
        
        RequestManager.singleton.requestGET(requestMethod: kAPIOrder, parameters: requestParameters!.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            if self.requestParameters?.typePay == "0" {
                
                let paymentParameter = RequestParamModal()
                paymentParameter.mobile = "0".appending((currentUser?.mobile)!)
                
                if let id_order : Any = response.value(forKey: "order_id") {
                    
                    self.orderId = Int(id_order as! String)
                    
                    let price : Double = Double(self.requestParameters!.price!)!
                    AppDelegate.singleton.service.generatePayAuth(Int(price), orderid: self.orderId!, callbackurl: "https://www.pec24.com/pecpaymentgateway", apikey: kPaymentAPIKey)
                }
                
                //let payAuth = self.service.generatePayAuth(Int(price)!, orderid: orderId as! Int, callbackurl: "https://itunes.apple.com/us/app/bebarobeshoor/id1212098925?ls=1&mt=8", apikey: "9A81F84B-4140-4C62-B1FE-7FB7D87F05ED")
                
                /*let controller = NavigationManager.singleton.screen(screenType: .BankInvoice) as! BankInvoiceViewController
                controller.invoiceNumber = String(describing: response.object(forKey: "order_id") ?? "0")
                self.navigationController?.pushViewController(controller, animated: true)*/
                
            } else {

                let controller = NavigationManager.singleton.screen(screenType: .CardInvoice) as! CardInvoiceViewController
                controller.invoiceNumber = String(describing: response.object(forKey: "order_id") ?? "0")
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            
        }) { (response : NSDictionary?, error : Error?) in
            
        }
    }
    
    //------------------------------------------------------
    
    //MARK: RCServiceDelegate
    
    func rcStartLoading() {
        AppDelegate.singleton.showLoader()
    }
    
    func rcStopLoading() {
        AppDelegate.singleton.hideLoader()
    }
    
    func rcCallBackSuccessPayAuth(_ PA: String!) {
        
        /*var resposne : NSDictionary?
        
        if let data = PA.data(using: .utf8) {
            do {
                resposne = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            } catch {
                print(error.localizedDescription)
            }
        }
        
        if resposne != nil {
            
            let payAuthModal = PayAuthModal(fromDictionary: resposne!)
            //service.openWebPayment(payAuthModal.result.data.payAuth, apikey: kPaymentAPIKey, controller: self)
        }*/
        
        AppDelegate.singleton.service.openWebPayment(PA, apikey: kPaymentAPIKey, controller: self)
    }
    
    func rcServiceError(_ Error: String!) {
        messageBox(message: Error)
    }
    
    func rcCallBackFailedPayAuth(_ Error: String!) {
        print(Error)
    }
    
    func rcCallBackFailedPaymentVerify(_ Error: String!) {
        print(Error)
        
        let parameter = RequestParamModal()
        parameter.orderId = String(describing: self.orderId)
        
        RequestManager.singleton.requestGET(requestMethod: kAPIOrderCancel, parameters: parameter.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            messageBox(message: "شما پرداخت با موفقیت لغو اند")
            _ = self.navigationController?.popToRootViewController(animated: true)
            
        }) { (response : NSDictionary?, error : Error?) in
            
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func rcCallBackSuccessPaymentVerify(_ Response: [AnyHashable : Any]!) {
        
        if self.orderId != nil {
            let controller = NavigationManager.singleton.screen(screenType: .BankInvoice) as! BankInvoiceViewController
            controller.invoiceNumber = String(describing: self.orderId)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    override func remoteControlReceived(with event: UIEvent?) {
        print(event)
    }
    
    func rcCallBackSuccessGetUserToken(_ Token: String!) {
        
    }
    
    func rcCallBackFailedGetUserToken(_ Error: String!) {
        
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //shadow to ui button
        payByCash.backgroundColor = UIColor.white
        payByCash.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        payByCash.layer.shadowOffset = CGSize(width: 0, height: 3)
        payByCash.layer.shadowOpacity = 1.0
        payByCash.layer.shadowRadius = 10.0
        payByCash.layer.masksToBounds = false
        payByCash.layer.cornerRadius = 4.0
        
        
        PayByCard.backgroundColor = UIColor.init(hex:"15C916")
        PayByCard.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        PayByCard.layer.shadowOffset = CGSize(width: 0, height: 3)
        PayByCard.layer.shadowOpacity = 1.0
        PayByCard.layer.shadowRadius = 10.0
        PayByCard.layer.masksToBounds = false
        PayByCard.layer.cornerRadius = 4.0
        
        
        continueB.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        continueB.layer.shadowOffset = CGSize(width: 0, height: 3)
        continueB.layer.shadowOpacity = 1.0
        continueB.layer.shadowRadius = 10.0
        continueB.layer.masksToBounds = false
        continueB.layer.cornerRadius = 5.0
        
        //AppDelegate.singleton.service.testMode = true
        AppDelegate.singleton.service.delegate = self
    
        let price : Double = Double(self.requestParameters!.price!)!
        let dis : Double = Double(self.requestParameters!.discount!)!
        
        lblTotalCost.layer.cornerRadius = 15
        lblTotalCost.layer.masksToBounds = true
        
        
        if (dis != 0){
            lblTotalCost.text = String(format: "%.f", (price*dis)/100)
        }else{
            lblTotalCost.text = String(format: "%.f", price)
        }
         self.requestParameters!.price = lblTotalCost.text
      
       
        infoView.backgroundColor = kColorGreen
                requestParameters?.typePay = "0"
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
  
 
    }
    
    //------------------------------------------------------
}
