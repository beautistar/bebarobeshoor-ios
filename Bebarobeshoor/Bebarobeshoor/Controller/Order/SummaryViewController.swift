//
//  SummaryViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/5/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class SummaryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblInvoiceNumber: BEBUILable!
    @IBOutlet weak var continueB: UIButton!
    
    let products : [ProductModal] = ProductManager.singleton.products
    
    var requestParameters : RequestParamModal?
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func initView() {
        
        self.title = "خلاصه سفارش"
        self.navigationItem.leftBarButtonItem?.image = UIImage.init(named: "back")
//        topView.backgroundColor = UIColor.green
        
        continueB.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        continueB.layer.shadowOffset = CGSize(width: 0, height: 3)
        continueB.layer.shadowOpacity = 1.0
        continueB.layer.shadowRadius = 5.0
        continueB.layer.masksToBounds = false
        continueB.layer.cornerRadius = 4.0
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueTap(_ sender: BEBUIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.Payment) as! PaymentViewController
        controller.requestParameters = requestParameters
        _ = navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SummaryCell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell", for: indexPath) as! SummaryCell
        let product = products[indexPath.row]
        cell.lblTitle.text = product.title
        let toArray = product.title.components(separatedBy: "<br>")
        if toArray.count >= 2{
            let backToString = toArray.joined(separator: "")
            cell.lblTitle.text = backToString
        }
        
        
        
        cell.lblPrice.text = String(Double(product.price)! * product.tickCount)
        return cell;
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initView()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------    
}

