//
//  CardInvoiceViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/5/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class CardInvoiceViewController: UIViewController {

    @IBOutlet weak var lblInvoiveNumber: BEBUILable!
    @IBOutlet weak var infoView: UIView!
    var invoiceNumber : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblInvoiveNumber.text = invoiceNumber ?? ""
        infoView.backgroundColor = UIColor.green
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnDoneTap(_ sender: BEBUIButton) {
        
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
}
