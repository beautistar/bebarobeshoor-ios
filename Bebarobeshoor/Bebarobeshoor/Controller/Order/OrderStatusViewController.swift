//
//  OrderStatusViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/8/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import TGPControls

class OrderStatusViewController : UIViewController {

    @IBOutlet weak var completedView: UIView!
    @IBOutlet weak var imgProgress: UIImageView!
    @IBOutlet weak var imgWashing: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    
    var orderDetail : OrderDetailModal!
    var orders : [OrderDetailModal]! = []
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
   
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        if orderDetail.state == "1" {
            imgProgress.image = #imageLiteral(resourceName: "order_progress1")
            imgWashing.image = #imageLiteral(resourceName: "order_washing1")
            completedView.isHidden = true
        }
        
        if orderDetail.state == "2" {
            imgProgress.image = #imageLiteral(resourceName: "order_progress2")
            imgWashing.image = #imageLiteral(resourceName: "order_washing1")
            completedView.isHidden = true
        }
        
        if orderDetail.state == "3" {
            imgProgress.image = #imageLiteral(resourceName: "order_progress3")
            imgWashing.image = #imageLiteral(resourceName: "order_washing3")
            completedView.isHidden = true
        }
        
        if orderDetail.state == "4" {
            imgProgress.image = #imageLiteral(resourceName: "order_progress4")
            imgWashing.image = #imageLiteral(resourceName: "order_washing4")
            completedView.isHidden = true
        }
        
        if orderDetail.state == "5" {
            imgProgress.image = #imageLiteral(resourceName: "order_progress_complete")
            imgWashing.image = #imageLiteral(resourceName: "order_washing4")
            imgWashing.isHidden = true
            completedView.isHidden = false
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnRemoveOrderTap(_ sender: UIButton) {
        
        let parameter = RequestParamModal()
        parameter.orderId = orderDetail.idOrder
        
        RequestManager.singleton.requestGET(requestMethod: kAPIOrderCancel, parameters: parameter.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            let index = self.orders.index(of: self.orderDetail)
            if index != nil {
                self.orders.remove(at: index!)
            }
            messageBox(message: "شما باید لغو سفارش موفقیت")
            _ = self.navigationController?.popViewController(animated: true)
            
        }) { (response : NSDictionary?, error : Error?) in
            
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

