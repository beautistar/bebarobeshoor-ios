//
//  IntroViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 1/31/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Foundation

class IntroViewController: BaseVC, UIScrollViewDelegate {
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var bgnLogin: UIButton!
    @IBOutlet weak var btnStartNow: UIButton!
    @IBOutlet weak var btnNewOrder: BEBUIButton!
    
    @IBOutlet weak var viewNewOrder: UIView!
    
    var imageCount:Int = 0;
    var pageNumber:Int = 0;
    var _scrollViewWidth:CGFloat = 0.0
    var _pageNumber:Int = 0;
    var isNewOrder : Bool = false
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnNewOrderTap(_ sender: BEBUIButton) {
        dismiss(animated: false, completion: nil)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationNewOrder"), object: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isNewOrder == true {
            viewNewOrder.isHidden = false
        }
        
        self.initView()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
        
    func initView() {
        
        print("screen width %f", self.view.frame.size.width)
        
        imageCount = 4;
        
        //scroll view
        
        scroll.backgroundColor = UIColor.clear;
        scroll.delegate = self;
        scroll.isPagingEnabled = true;
        scroll.alwaysBounceVertical = false;
        scroll.alwaysBounceHorizontal = false;
        scroll.bounces = false;
        scroll.showsHorizontalScrollIndicator = false
        scroll.contentSize = CGSize(width:CGFloat(imageCount) * self.view.frame.size.width, height:self.view.frame.size.height - 50.0);
        
        // page control
        
        pageControl.backgroundColor = UIColor.clear;
        pageControl.numberOfPages = imageCount;
        pageControl.addTarget(self, action: #selector(pageChanged), for: UIControlEvents.valueChanged);
        
        var x: CGFloat = 0.0

        for i in 1..<imageCount + 1 {
            
            let view = PageView.fromNib() as PageView
    
            view.frame = CGRect(x: x+0, y: 0, width: scroll.frame.size.width, height: scroll.frame.size.height);
            view.imvBg.image = UIImage.init(named: String(format: "bg_step%d", i-1))
            view.imvIcon.image = UIImage.init(named: String(format: "icon_step%d", i-1))
            
            switch i {
            case 1:
                view.lblTitle.text = "Laundry & Dry Cleaning Made Easy".localized()
                view.lblAbout.text = "We pickup, wash and deliver.".localized()
                break
            case 2:
                view.lblTitle.text = "1. Book with our app".localized()
                view.lblAbout.text = "Choose a convenient pickup time.".localized()
                break
            case 3:
                view.lblTitle.text = "2. Meet our Pickup Pilot".localized()
                view.lblAbout.text = "We collect your dirty clothes from your doorstep.".localized()
                break
            default:
                view.lblTitle.text = "3. Dress up Next Day".localized()
                view.lblAbout.text = "We bring your fresh clothes back the very next day.".localized()
                break
            }
            
            scroll.addSubview(view)
            x+=self.view.frame.size.width;
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        _scrollViewWidth = scrollView.frame.size.width;
        _pageNumber = Int(floor((scrollView.contentOffset.x - _scrollViewWidth/50) / _scrollViewWidth)) + 1;
        pageControl.currentPage = _pageNumber;
    }
    
    func pageChanged() {
        
        let pageNumber = pageControl.currentPage;
        
        var frame:CGRect = scroll.frame;
        frame.origin.x = (frame.size.width * CGFloat(pageNumber))
        frame.origin.y = 0.0;
        scroll.scrollRectToVisible(frame, animated: true)
        
    }
    
}

