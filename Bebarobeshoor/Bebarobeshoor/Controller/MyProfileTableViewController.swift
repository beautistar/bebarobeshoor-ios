//
//  MyProfileTableViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/5/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import AFNetworking

class MyProfileTableViewController: UITableViewController {
    var orderViewController: UIViewController!
   
    @IBOutlet weak var lblName: BEBUILable!
    @IBOutlet weak var lblDiscount: BEBUILable!
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func requestToGetProfile() {
    
        let parameters = RequestParamModal()
        parameters.customerId = currentUser?.userid
        
        RequestManager.singleton.requestGET(requestMethod: kAPIProfile, parameters: parameters.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            let messageModal = MessageModalUser(fromDictionary: response)
            let user = messageModal.customerData!
            user.userid = currentUser?.userid
            print(user.toDictionary())
            
            let userData = NSKeyedArchiver.archivedData(withRootObject: user.toDictionary())
            MyUserDefaults.setValue(userData, forKey: kCurrentUser)
            
            self.configureData()
            
        }) { (response : NSDictionary?, error : Error?) in
            
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureData() {
        
        let prefix = "Hello Mr".localized().appending(" ")
        let userName = prefix.appending(currentUser?.firstname ?? "").appending(currentUser?.lastname ?? "")
        
        lblName.text = userName
        if currentUser?.discount != nil && (currentUser?.discount.characters.count)! > 0{
            lblDiscount.text = "Discount ".appending(currentUser!.discount!).appending("%")
        }  else {
            lblDiscount.text = ""
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func backAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNewOrderTap(_ sender: BEBUIButton) {
        let orderViewController = NavigationManager.singleton.screen(screenType: NavigationScreenType.Home)
        self.orderViewController = UINavigationController(rootViewController: orderViewController)
        self.slideMenuController()?.changeMainViewController(self.orderViewController, close: true)
//        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.Home) as! MainViewController
//        controller.isFromNewOrder = true
//        _ = navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        
        MyUserDefaults.removeObject(forKey: kCurrentUser)
        MyUserDefaults.synchronize()
        NavigationManager.singleton.setupLogin()
    }
    
    @IBAction func btnEditProfileTap(_ sender: BEBUIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.AddContact) as! AddContactViewController
        controller.isEditProfile = true
        _ = navigationController?.pushViewController(controller, animated: true)        
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "My Profile".localized()
        
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange { (status : AFNetworkReachabilityStatus) in
            
            switch status {
                case .reachableViaWiFi:
                    self.requestToGetProfile()
                    break
                case .reachableViaWWAN:
                    self.requestToGetProfile()
                    break
                default:
                    break
            }
        }
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigationBarItem()
        
        if AFNetworkReachabilityManager.shared().isReachable {
            self.requestToGetProfile()
        }
    }
    
    //------------------------------------------------------
}

extension MyProfileTableViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
