//
//  WashIronViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/3/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import AFNetworking

class WashIronViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionProduct: UICollectionView!
    
    var rootVC:  PricingViewController? = nil
    var products : [ProductModal] = []
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        cell.rootVC = rootVC
        let product = products[indexPath.row]
        cell.product = product
        let imgProductURL = URL(string: product.image)
        if (imgProductURL != nil) {
            cell.imgProduct.setImageWith(imgProductURL!, placeholderImage: UIImage())
        }
        cell.lblProductName.text = product.title
        let toArray = product.title.components(separatedBy: "<br>")
        if toArray.count >= 2{
            let backToString = toArray.joined(separator: "")
            cell.lblProductName.text = backToString
        }
        cell.lblProductPrice.text = product.price
        cell.stepper.value = product.tickCount ?? 0
        
        if cell.stepper.value == 0.0 {
            cell.unMarkAsSelectedProduct()
        } else {
            cell.markAsSelectedProduct()
        }
        return cell
    }
    
    //------------------------------------------------------
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {             
        
        let cell = collectionView.cellForItem(at: indexPath) as! ProductCell
        cell.stepper.value = cell.stepper.value + 1.0
        
        if cell.stepper.value == 0.0 {
            cell.unMarkAsSelectedProduct()
        } else {
            cell.markAsSelectedProduct()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let cellWidth = self.view.frame.size.width / 2.0 - 5;
        return CGSize(width: cellWidth, height: cellWidth * 2)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1;
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}



