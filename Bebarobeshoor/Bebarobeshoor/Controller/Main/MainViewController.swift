//
//  MainViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/1/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import TGPControls
import SwiftDate

class MainViewController: UIViewController, TableStatic1Delegate, TableStatic2Delegate, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet var textInputView: UIView!
    @IBOutlet weak var tblLeftList: UITableView!
    @IBOutlet weak var tblRightList: UITableView!
    @IBOutlet weak var tblMainList: UITableView!
    
    let numberOfDays = 30
    let numberOfTimeSlots = 24
    
    let operationQueue = OperationQueue()
    
    var packages : [PackageModal]!
    var hours : [HoursModal]!
    var dates : [DatesModal]!
    
    var discountApi : String = "https://bebarobeshoor.com/index.php/Api_discount?code="
 
    
    var requestParameters = RequestParamModal()
    
    var isFromNewOrder : Bool = false
    
    //------------------------------------------------------
    
    //MARK: Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }*/
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func calcelAllOperation() {
        
        DispatchQueue.main.async {
        
            self.operationQueue.cancelAllOperations()
            AppDelegate.singleton.hideLoader()
            self.tblMainList.isHidden = false
        }
    }
    
    func requestPackages() {
    
        RequestManager.singleton.requestGET(requestMethod: kAPIPackages, parameters: [:], showLoader: false, successBlock: { (response : NSDictionary) in
            
            let messageModal = MessageProductModal(fromDictionary: response)
            self.packages = messageModal.packages
            
            DispatchQueue.main.async {
                self.tblMainList.reloadData()
            }
            
        }) { (response : NSDictionary?, error : Error?) in
            
            self.calcelAllOperation()
        }
    }
    
    func requestDates() {
        
        RequestManager.singleton.requestGET(requestMethod: kAPIDates, parameters: [:], showLoader: false, successBlock: { (response : NSDictionary) in
            
            let messageModal = MessageProductModal(fromDictionary: response)
            self.dates = messageModal.dates
            
            DispatchQueue.main.async {
                self.tblLeftList.reloadData()
            }
            
        }) { (response : NSDictionary?, error : Error?) in
            
           self.calcelAllOperation()
        }
    }

    func requestHours() {
        
        RequestManager.singleton.requestGET(requestMethod: kAPIHours, parameters: requestParameters.toDictionary() as! [String : AnyObject], showLoader: false
            , successBlock: { (response : NSDictionary) in
                
                //let messageModal = MessageProductModal(fromDictionary: response)
                let messageModal = MessageModalHours(fromDictionary: response)
                self.hours = messageModal.hours
                
                DispatchQueue.main.async {
                    
                    self.tblRightList.reloadData()
                    self.tblMainList.reloadData()
                    self.tblMainList.isHidden = false
                    AppDelegate.singleton.hideLoader()
                }
                
        }) { (response : NSDictionary?, error : Error?) in
            
            self.calcelAllOperation()
        }
    }

    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnPickupTap(_ sender: UIButton) {
    }
    
    @IBAction func btnDropOffTap(_ sender: UIButton) {
    }
    
    @IBAction func btnContinueTap(_ sender: BEBUIButton) {
        let indexPath = IndexPath(row: 2, section: 0)
 
        self.requestParameters.descript = (tblMainList.cellForRow(at: indexPath) as! TableStatic3).txtComment.text ?? ""

        let indexPath2 = IndexPath(row: 4, section: 0)
        //Comment this line and uncomment above line
        let copon = (tblMainList.cellForRow(at: indexPath2) as! TableStatic4).txtCopon.text ?? "0"
        let urlSting : String = "\(discountApi)\(copon)"
        self.discountApiCall(discount: urlSting)
         DispatchQueue.main.async {
            self.move()
        }
    }
    func discountApiCall(discount: String){
        let urlString = URL(string:discount)
        if let url = urlString {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error ?? "")
                } else {
                    do {
                        let parseJSON = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: AnyObject]
                        let names = parseJSON["response"]?.object(forKey: "discount")
                        self.requestParameters.discount = names as! String
                        
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
                
            }
            task.resume()
        }
    }
    
    func move(){
        
            let indexPath = IndexPath(row: 4, section: 0)
            self.requestParameters.copon = (tblMainList.cellForRow(at: indexPath) as! TableStatic4).txtCopon.text ?? "0"
            
            let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.PriceCalculator) as! PricingViewController
            controller.isFromMyPanel = true
            controller.requestParameters = requestParameters
            _ = navigationController?.pushViewController(controller, animated: true)
    }
    
       
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        
            case tblLeftList:
                return dates.count
            case tblRightList:
                return hours.count
            case tblMainList:
                return 6
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tblLeftList == tableView {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "leftCell", for: indexPath)
            cell.selectionStyle = .default
            cell.selectedBackgroundView = BlankView
            let indexDate = dates[indexPath.row]
            cell.textLabel?.text = indexDate.formatedDate! as String
            cell.textLabel?.highlightedTextColor = kColorGreen
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
            cell.textLabel?.textAlignment = .right
    
            var t4 : String? = nil
            if let theTitle = indexDate.formatedDate {
                t4 = theTitle
            }
            cell.textLabel?.text = "\(t4 ?? "")"
            
             return cell
        }
        
        if tblRightList == tableView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "rightCell", for: indexPath)
            cell.selectionStyle = .default
            cell.selectedBackgroundView = BlankView
            cell.textLabel?.text = hours[indexPath.row].time
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            cell.textLabel?.textAlignment = .right
            cell.detailTextLabel?.text = ""
            cell.textLabel?.highlightedTextColor = kColorGreen
            cell.detailTextLabel?.highlightedTextColor = kColorGreen
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(indexPath.row + 1))!
        cell.selectionStyle = .none
        
        if cell is TableStatic1 {
            let staticCell1 = cell as! TableStatic1
            staticCell1.delegate = self
            staticCell1.rootVC = self
            staticCell1.packages = packages
            staticCell1.configureUI()
        }
        
        if cell is TableStatic2 {
            let staticCell2 = cell as! TableStatic2
            staticCell2.delegate = self
            staticCell2.rootVC = self
            staticCell2.hour = hours?.first
            staticCell2.dates = dates?.first
            staticCell2.configureUI()
        }
        return cell
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDelegate 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tblMainList == tableView {
            
            switch indexPath.row {
            case 0:
                return 230
            case 1:
                return 125
            case 2:
                return 55
            case 3:
                return 40
            case 4:
                return 55
            case 5:
                return 80
            default:
                return 55
            }
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if tblLeftList == tableView {
            
            let indexDate = dates[indexPath.row]
            let cell = tblMainList.cellForRow(at: IndexPath(row: 1, section: 0)) as! TableStatic2
            var t4 : String? = nil
            if let theTitle = indexDate.formatedDate {
                t4 = theTitle
            }

            if cell.txtPickup.isFirstResponder {
                cell.lblDay.text = "\(t4 ?? "")"
                requestParameters.pickupDate = indexDate.date
            }
            
            if cell.txtDropup.isFirstResponder {
                cell.lblDate.text = "\(t4 ?? "")"
                requestParameters.dropOffDate = indexDate.date
            }
            self.requestHours()
            
            
        }
        
        if tblRightList == tableView {
            
            let timeSlot = hours[indexPath.row].time
            let cell = tblMainList.cellForRow(at: IndexPath(row: 1, section: 0)) as! TableStatic2
            
            if cell.txtPickup.isFirstResponder {
                cell.txtPickup.text = timeSlot
            }
            if cell.txtDropup.isFirstResponder {
                cell.txtDropup.text = timeSlot
            }
        }
        
        if tblMainList == tableView {
        }
    }
    
    //------------------------------------------------------
    
    //MARK: TableStatic1Delegate
    
    func imgSlierValueDidChange(sender: TGPDiscreteSlider) {
        switch sender.tag {
        case 0:
            break;
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        default:
           break
        }
    }
    
    //------------------------------------------------------
    
    //MARK: TableStatic2Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.inputView = textInputView
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromNewOrder {
            self.title = "New Order".localized()
        } else {
            self.title = "Order".localized()
        }
        
        operationQueue.maxConcurrentOperationCount = 1
        
        /*for index in 0..<numberOfDays {
            arrDayDates.append(Date().add(components: [Calendar.Component.day : index]))
        }
        
        let date = Date()
        let cal = Calendar(identifier: .gregorian)
        let startDate = cal.startOfDay(for: date)
        let endDate = startDate.add(components: [Calendar.Component.day : 1])
        
        var dateComponents = DateComponents()
        dateComponents.hour = 2
        let slotDates = Date.dates(between: startDate, and: endDate, increment: dateComponents)
        for indexDate in slotDates {
            
            if !indexDate.isInPast && indexDate.isToday {
                
                let startTime = indexDate.string(custom: kNewOrderTimeFormate)
                let endTime = indexDate.add(components: [Calendar.Component.minute : 30]).string(custom: kNewOrderTimeFormate)
                arrTimeSlots.append("\(startTime) - \(endTime)")
            }
        }*/
        
        self.tblMainList.isHidden = true
        AppDelegate.singleton.showLoader()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            
            self.operationQueue.addOperation {
                self.requestPackages()
            }
            self.operationQueue.addOperation {
                self.requestDates()
            }
            self.operationQueue.addOperation {
                self.requestHours()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFromNewOrder {
            
        } else {
            
            self.setNavigationBarItem()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

////////
//
//  playButton.setImage(UIImage(named: "sc_persil_order_summary_ic"), forState: UIControlState.Normal)
//

protocol TableStatic1Delegate {
    
    func imgSlierValueDidChange(sender : TGPDiscreteSlider)
}


class TableStatic1: UITableViewCell {
    
    @IBOutlet weak var titleSlider: TGPCamelLabels!
    @IBOutlet weak var imgSlider: TGPDiscreteSlider!
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var lblMinutes: BEBUILable!
    @IBOutlet weak var lblHours: BEBUILable!
    @IBOutlet weak var lblWash: BEBUILable!
    @IBOutlet weak var lblServicePrice: UILabel!
    
    var delegate : TableStatic1Delegate?
    var packages : [PackageModal]?
    var rootVC:  MainViewController? = nil
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        let result = packages?.map({ (indexPackage : PackageModal) -> String in
            return indexPackage.name
        })
        
        if result != nil {
            
            self.titleSlider.names = result
            self.imgSlider.tickCount = Int32(result!.count)
            self.imgSlider.tickCount = Int32(result!.count)
            
            self.imgSlider.value = 0//CGFloat((self.titleSlider.names.count + 1) * -1)
            self.titleSlider.value = UInt(self.titleSlider.names.count - 1)
            self.subView.layer.borderColor = UIColor.clear.cgColor;
            configureText(package: packages?.last)
        }
    }
    
    func configureText(package : PackageModal?) {
        
        if package != nil {
            
            rootVC?.requestParameters.idPackage = package?.idPackage
            
            lblMinutes.text = "\(package!.timeSlot!) ".appending("minute time slot".localized())
            lblHours.text = "\(package!.timeTurnaround!) ".appending("hour turnaround".localized())
            lblWash.text = ("Washed with".localized()).appending(" \(package!.name!)")
            lblServicePrice.text = "\(package!.serviceFee!)"
        }
    }
    
    func imgSlierValueDidChange(sender : TGPDiscreteSlider) {
        
        let index = UInt((sender.value + 1) * -1)
        let titleSiderIndex = UInt(self.titleSlider.names.count) - index
        self.titleSlider.value = titleSiderIndex
        
        sender.tag = Int(titleSiderIndex)
        let package = packages?[sender.tag]
        configureText(package: package)
        delegate?.imgSlierValueDidChange(sender: sender)
    }
    
    //------------------------------------------------------
    
    //MARK: Initialisation
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgSlider.addTarget(self, action: #selector(imgSlierValueDidChange), for: UIControlEvents.valueChanged)
    }
}

protocol TableStatic2Delegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField)
}

class TableStatic2: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var txtPickup: BEBUITextField!
    @IBOutlet weak var txtDropup: BEBUITextField!
    @IBOutlet weak var lblDay: BEBUILable!
    @IBOutlet weak var lblDate: BEBUILable!
    @IBOutlet weak var btnContinue: BEBUIButton!
    
    var delegate : TableStatic2Delegate?
    var hour : HoursModal?
    var dates : DatesModal?
    var rootVC:  MainViewController? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureUI() {
        
        if txtPickup.text?.isEmpty == true {
            txtPickup.text = hour?.time
        }
        
        if txtDropup.text?.isEmpty == true {
            txtDropup.text = hour?.time
        }
        
        if lblDay.text?.characters.count == 0 && dates != nil {
           // lblDay.text = dates?.day
        }
        
        if lblDate.text?.characters.count == 0 && dates != nil  {
            //lblDate.text = try! dates?.date.date(format: DateFormat.custom(kReponseDateFormat)).string(custom: kNewOrderDateFormate)
            rootVC?.requestParameters.pickupDate = dates?.date
            rootVC?.requestParameters.dropOffDate = dates?.date
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (txtPickup == textField || txtDropup == textField) {
            delegate?.textFieldDidBeginEditing(textField)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (txtPickup == textField || txtDropup == textField) {
            return false
        }
        return true
    }
}

class TableStatic3: UITableViewCell {
    
    @IBOutlet weak var txtComment: BEBUITextField!
}

class TableStatic4: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var txtCopon: BEBUITextField!
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        let tText = textField.text as NSString?
//        let fullText = tText?.replacingCharacters(in: range, with: string)
//        
//        if (fullText?.length)! > 4 {
//            return false
//        } else {
//            return true
//        }
        return true
    }
    
    //------------------------------------------------------
    
    //MARK: Inititalisation 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtCopon.delegate = self
    }
}
