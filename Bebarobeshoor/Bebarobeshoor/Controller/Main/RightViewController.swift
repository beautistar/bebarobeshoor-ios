//
//  LeftViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/1/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

enum RightMenu: Int {
    case main = 0
    case order
    case pricing
    case help
    case howit
    case legal
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: RightMenu)
}

class RightViewController: UIViewController,UIActionSheetDelegate {

    @IBOutlet weak var tblMenu: UITableView!
    var menus = ["My Panel", "Order", "Pricing", "Help", "How it works", "خروج از سیستم"]
    var mainViewController: UIViewController!
    var orderViewController: UIViewController!
    var pricingViewController: UIViewController!
    var helpViewController: UIViewController!
    var howItViewController: UIViewController!
    var legalViewController: UIViewController!
   
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tblMenu.tableFooterView = UIView.init()
        self.tblMenu.bounces = false;
        
//        self.tblMenu.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        self.tblMenu.separatorColor = UIColor.clear
        
        let mainViewController = NavigationManager.singleton.screen(screenType: NavigationScreenType.MyProfile)
        self.mainViewController = UINavigationController(rootViewController: mainViewController)
        
        let orderViewController = NavigationManager.singleton.screen(screenType: NavigationScreenType.Home)
        self.orderViewController = UINavigationController(rootViewController: orderViewController)
        
        let pricingViewController = NavigationManager.singleton.screen(screenType: NavigationScreenType.PriceCalculator)
        self.pricingViewController = UINavigationController(rootViewController: pricingViewController)
        
        let helpViewController = NavigationManager.singleton.screen(screenType: NavigationScreenType.Help)
        self.helpViewController = UINavigationController(rootViewController: helpViewController)
        
        let howItController = NavigationManager.singleton.screen(screenType: NavigationScreenType.HowIt)
        //nonMenuController.delegate = self
        self.howItViewController = UINavigationController(rootViewController: howItController)
        
        let legalViewController = NavigationManager.singleton.screen(screenType: NavigationScreenType.Legal)
        self.legalViewController = UINavigationController(rootViewController: legalViewController)
        
        self.tblMenu.registerCellClass(MenuCell.self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 160)
        self.view.layoutIfNeeded()
    }
    
    func changeViewController(_ menu: RightMenu) {
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .order:
            self.slideMenuController()?.changeMainViewController(self.orderViewController, close: true)
        case .pricing:
            self.slideMenuController()?.changeMainViewController(self.pricingViewController, close: true)
        case .help:
            self.slideMenuController()?.changeMainViewController(self.helpViewController, close: true)
        case .howit:
            self.slideMenuController()?.changeMainViewController(self.howItViewController, close: true)
        case .legal:
            self.slideMenuController()?.changeMainViewController(self.legalViewController, close: true)
        }
    }
}

extension RightViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = RightMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .order, .pricing, .help, .howit, .legal:
                return MenuCell.height()
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
            MyUserDefaults.removeObject(forKey: kCurrentUser)
            MyUserDefaults.synchronize()
            NavigationManager.singleton.setupLogin()
            return;
        }else if indexPath.row == 3{
            let actionSheetControllerIOS8: UIAlertController = UIAlertController(title:"لطفا انتخاب کنید", message: "", preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "لغو کردن", style: .cancel) { _ in
                print("Cancel")
            }
            actionSheetControllerIOS8.addAction(cancelActionButton)
            
            let saveActionButton = UIAlertAction(title: "با ما تماس بگیرید", style: .default)
            { _ in
                if UIApplication.shared.canOpenURL(URL(string: "tel://\(02122732580)")!){
                    UIApplication.shared.openURL(URL(string: "tel://\(02122732580)")!)
                }
            }
            actionSheetControllerIOS8.addAction(saveActionButton)
            
            let deleteActionButton = UIAlertAction(title: "تماس با ما", style: .default)
            { _ in
                 //https://telegram.me/info_bebarobeshoor
                UIApplication.shared.openURL(URL(string: "https://telegram.me/info_bebarobeshoor")!)
                
            }
            actionSheetControllerIOS8.addAction(deleteActionButton)
            self.present(actionSheetControllerIOS8, animated: true, completion: nil)
            return;
        }
        
        if let menu = RightMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tblMenu == scrollView {
            
        }
    }
}

extension RightViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 5 {
             let cell = MenuCell(style: UITableViewCellStyle.default, reuseIdentifier: MenuCell.identifier)
            cell.textLabel?.textAlignment = NSTextAlignment.right
            cell.textLabel?.text = "خروج از سیستم"
            cell.backgroundColor = UIColor.clear
            cell.textLabel?.textColor = UIColor.white
            return cell;
        }
        
        
        if let menu = RightMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .order, .pricing, .help, .howit, .legal:
                let cell = MenuCell(style: UITableViewCellStyle.default, reuseIdentifier: MenuCell.identifier)
                cell.textLabel?.textAlignment = NSTextAlignment.right
                cell.setData(menus[indexPath.row].localized())
                return cell
            }
        }
        return UITableViewCell()
    }
}
