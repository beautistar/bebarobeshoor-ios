//
//  ReferFriendViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/8/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class ReferFriendViewController: UIViewController {

    @IBOutlet weak var lblInviteText: BEBUILable!
    @IBOutlet weak var txtPhoneNumber: BEBUITextField!
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func requestInviteFriend() {
        
        let parameters = RequestParamModal()
        parameters.userUndId = currentUser?.userid
        parameters.mobile = txtPhoneNumber.text
        
        RequestManager.singleton.requestGET(requestMethod: kAPIReferral, parameters: parameters.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            messageBox(message: "شما با موفقیت دعوت کردن از دوست خود")
            _ = self.navigationController?.popViewController(animated: true)
            
        }) { (response : NSDictionary?, error : Error?) in
            
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func IsValidAllInputs() -> Bool {
        
        if txtPhoneNumber.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtPhoneNumber)
            return false
        }
        
        return true
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnInviteTap(_ sender: BEBUIButton) {
        
        if IsValidAllInputs() == false {
            return
        }
        
        requestInviteFriend()
    }
    
    @IBAction func backAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Referral Friend
        self.title = "دعوت از دوستان"
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

