//
//  PricingViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/2/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import CarbonKit

class PricingViewController: UIViewController, CarbonTabSwipeNavigationDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    var searchController : UISearchController!
    
    @IBOutlet weak var collectionSearch: UICollectionView!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoViewBottonConstraint: NSLayoutConstraint!
    @IBOutlet weak var imvDrop: UIImageView!
    @IBOutlet weak var layoutInfoHeight: NSLayoutConstraint!
    @IBOutlet weak var tblProduct: UITableView!
    @IBOutlet weak var lblTotalPrice: BEBUILable!
    @IBOutlet weak var btnOrderNow: BEBUIButton!
    
    var washIronVC : WashIronViewController!
    var washGoldVC : WashFoldViewController!
    var dryCleanVC : DryCleanViewController!
    var linenBedVC : LimenBedViewController!
    
    var isShown:Bool = false;
    var isOpened:Bool = false;
    var items = NSArray()
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation = CarbonTabSwipeNavigation()

    var isFromMyPanel : Bool = false

    var products : [ProductModal] = []
    var searchProducts : [ProductModal] = []
    
    var requestParameters : RequestParamModal?
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func style() {
        
        let color = UIColor.green
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barStyle = .blackTranslucent
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.setIndicatorColor(color)
        carbonTabSwipeNavigation.setTabExtraWidth(30)
        carbonTabSwipeNavigation.setIndicatorHeight(2)
        carbonTabSwipeNavigation.setTabBarHeight(50)
        
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(130, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(130, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(130, forSegmentAt: 2)
       // carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(150, forSegmentAt: 3)
        
        carbonTabSwipeNavigation.setNormalColor(UIColor.black.withAlphaComponent(0.6))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.black, font: UIFont.boldSystemFont(ofSize: 14))
    }
    
    func requestToProduct() {
    
        RequestManager.singleton.requestGET(requestMethod: kAPIProductList, parameters: [:], showLoader: true, successBlock: { (response : NSDictionary) in
            
            let messageModal = MessageProductModal(fromDictionary: response)
            self.products = messageModal.products
            self.updateProduct()
            
        }) { (response : NSDictionary?, error : Error?) in
            
        }
    }
    
    func updateProduct() {
        
        self.washIronVC?.products = self.products.filter({ (indexProduct : ProductModal) -> Bool in
            if indexProduct.category == "1" {
                return true
            }
            return false
        })
        self.washIronVC?.collectionProduct.reloadData()
        
        self.washGoldVC?.products = self.products.filter({ (indexProduct : ProductModal) -> Bool in
            if indexProduct.category == "2" {
                return true
            }
            return false
        })
        
        self.washGoldVC?.collectionProduct.reloadData()
        
        self.dryCleanVC?.products = self.products.filter({ (indexProduct : ProductModal) -> Bool in
            if indexProduct.category == "3" {
                return true
            }
            return false
        })
        self.dryCleanVC?.collectionProduct.reloadData()
        
        self.linenBedVC?.products = self.products.filter({ (indexProduct : ProductModal) -> Bool in
            if indexProduct.category == "4" {
                return true
            }
            return false
        })
        self.linenBedVC?.collectionProduct.reloadData()
    }
    
    func searchTap(barButton : UIBarButtonItem) {
        present(searchController, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func showAction(_ sender: Any) {
        isOpened = !isOpened
        tblProduct.reloadData()
    }
    
    @IBAction func orderAction(_ sender: Any) {
        
        requestParameters?.idCustomer = currentUser?.userid
        requestParameters?.createDate = Date().string(custom: kReponseDateFormat)
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.OrderSummary) as! SummaryViewController
        controller.requestParameters = requestParameters
        _ = navigationController?.pushViewController(controller, animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: CarbonTabSwipeNavigation
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        switch index {
            
        case 0:
            washIronVC = self.storyboard!.instantiateViewController(withIdentifier: "WashIronViewController") as! WashIronViewController
            washIronVC.rootVC = self
            return washIronVC;
        case 1:
            washGoldVC = self.storyboard!.instantiateViewController(withIdentifier: "WashFoldViewController") as! WashFoldViewController
            washGoldVC.rootVC = self
            return washGoldVC
        case 2:
            dryCleanVC = self.storyboard!.instantiateViewController(withIdentifier: "DryCleanViewController") as! DryCleanViewController
            dryCleanVC.rootVC = self
            return dryCleanVC
        default:
            linenBedVC = self.storyboard!.instantiateViewController(withIdentifier: "LimenBedViewController") as! LimenBedViewController
            linenBedVC.rootVC = self
            return linenBedVC
        }
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        updateProduct()
    }
    
    //------------------------------------------------------
    
    //MARK: UITabkeViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count =  ProductManager.singleton.products.count
        let barHeiht : CGFloat = 50.0
        
        if isOpened {
            var height : CGFloat = CGFloat(count) * 80.0
            
            if height > (containerView.bounds.height - barHeiht) {
                height = containerView.bounds.height - barHeiht
            }
            layoutInfoHeight.constant = height + barHeiht
            imvDrop.transform = CGAffineTransform(rotationAngle: CGFloat(0));
            
        } else {
            
            let height : CGFloat = 50.0
            
            if count > 0 {
                infoView.isHidden = false
                infoViewBottonConstraint.constant = 0;
                imvDrop.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI));
            } else {
                infoView.isHidden = true
                infoViewBottonConstraint.constant = (-infoView.bounds.height) + 50;
                imvDrop.transform = CGAffineTransform(rotationAngle: CGFloat(0));
            }
            layoutInfoHeight.constant = height
        }
        
        let total = ProductManager.singleton.products.map { (indexProduct : ProductModal) -> Double in
            let price = Double(indexProduct.price) ?? 0
            let quantity = Double(indexProduct.tickCount)
            return price * quantity
        }
        
        lblTotalPrice.text = String(total.reduce(0, +))
        requestParameters?.price =  lblTotalPrice.text
        
        if ProductManager.singleton.products.count == 0 {
            btnOrderNow.isEnabled = false
            btnOrderNow.backgroundColor = UIColor.groupTableViewBackground
        } else {
            btnOrderNow.isEnabled = true
            btnOrderNow.backgroundColor = kColorGreen
        }
        return ProductManager.singleton.products.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ProductTblCell = tableView.dequeueReusableCell(withIdentifier: "ProductTblCell", for: indexPath) as! ProductTblCell
        cell.rootVC = self
        
        let product = ProductManager.singleton.products[indexPath.row]
        cell.product = product
        cell.lblProductName.text = product.title
        cell.lblPrice.text = product.price
        
        if let imgProductURL = URL(string: product.image) {
         cell.imgProduct.setImageWith(imgProductURL, placeholderImage: UIImage())
        }
        cell.stepper.value = product.tickCount
        
        return cell;
    }
   
    //------------------------------------------------------
    
    //MARK: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count > 0 {
            containerView.isHidden = true
        } else {
            containerView.isHidden = false
        }
        
        searchProducts = products.filter({ (indexProduct : ProductModal) -> Bool in
            
            if indexProduct.category == String(carbonTabSwipeNavigation.currentTabIndex + 1) && indexProduct.title.contains(searchText) {
                return true
            }
            return false
        })
        collectionSearch.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        containerView.isHidden = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        containerView.isHidden = false
    }
    
    //------------------------------------------------------
    
    //MARK: UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    //------------------------------------------------------
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        cell.rootVC = self
        let product = searchProducts[indexPath.row]
        cell.product = product
        let imgProductURL = URL(string: product.image)
        if (imgProductURL != nil) {
            cell.imgProduct.setImageWith(imgProductURL!, placeholderImage: UIImage())
        }
        cell.lblProductName.text = product.title
        let toArray = product.title.components(separatedBy: "<br>")
        if toArray.count >= 2{
            let backToString = toArray.joined(separator: "")
            cell.lblProductName.text = backToString
        }
        
        
        cell.lblProductPrice.text = product.price
        cell.stepper.value = product.tickCount ?? 0
        
        if cell.stepper.value == 0.0 {
            cell.unMarkAsSelectedProduct()
        } else {
            cell.markAsSelectedProduct()
        }
        return cell
    }
    
    //------------------------------------------------------
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let cell = collectionView.cellForItem(at: indexPath) as! ProductCell
        cell.stepper.value = cell.stepper.value + 1.0
        
        if cell.stepper.value == 0.0 {
            cell.unMarkAsSelectedProduct()
        } else {
            cell.markAsSelectedProduct()
        }        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    
        let reusableView = UICollectionReusableView()
        
        if kind == UICollectionElementKindSectionHeader {
            reusableView.addSubview(searchController.searchBar)
        }
        return reusableView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let cellWidth = self.view.frame.size.width / 2.0 - 5;
        return CGSize(width: cellWidth, height: cellWidth * 2)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1;
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProductManager.singleton.products.removeAll()
        
        // Do any additional setup after loading the view.
        searchController = UISearchController(searchResultsController: nil)
        imvDrop.transform = CGAffineTransform(rotationAngle: CGFloat(0));
        
        collectionSearch.dataSource = self
        collectionSearch.delegate = self
        
        searchController.searchBar.delegate = self
        infoViewBottonConstraint.constant = -100
        infoView.isHidden = true
        searchController.searchBar.placeholder = "جستجو"
//        let cancelButton = searchController.searchBar.value(forKey: "cancelButton") as! UIButton
//        cancelButton.setTitle("Done", for: .normal)
        
        searchController.searchBar.setValue("انصراف", forKey:"_cancelButtonText")
        
        self.navigationItem.title = "Pricing".localized()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(searchTap))
            
        items = ["WASH & IRON".localized(), "WASH & FOLD".localized(), "DRY CLEANING".localized()]
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)
        
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: containerView)
        self.style()
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        requestToProduct()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFromMyPanel {
            
        } else {
            
            self.setNavigationBarItem()
        }
    }

    //------------------------------------------------------
    
}
