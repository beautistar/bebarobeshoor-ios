//
//  HowItViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/2/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import MediaPlayer
import Foundation
import AVKit

class HowItViewController: UIViewController {

    var moviePlayer:MPMoviePlayerController!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        //howitworks.mov
        let moviePath = Bundle.main.path(forResource: "howitworks", ofType: "mov")
        let url = NSURL.fileURL(withPath: moviePath!)
        moviePlayer = MPMoviePlayerController(contentURL: url)
        moviePlayer.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(moviePlayer.view)
        moviePlayer.isFullscreen = true
        moviePlayer.controlStyle = MPMovieControlStyle.embedded
        
//        let moviePath = Bundle.main.path(forResource: "howitworks", ofType: "mov")
//        if let path = moviePath {
//            let url = NSURL.fileURL(withPath: path)
//            let player = AVPlayer.init(url:url)
//            let playerViewController = AVPlayerViewController()
//            playerViewController.player = player
//            self.present(playerViewController, animated: true) {
//                if let validPlayer = playerViewController.player {
//                    validPlayer.play()
//                }
//            }
//        }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = "چگونه کار میکنیم"
    }
    
    override func viewDidDisappear(_ animated: Bool) {
      moviePlayer.stop()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
