//
//  PhoneNumberViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 1/31/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class PhoneNumberViewController: BaseNavigationVC {

    @IBOutlet weak var txtPhoneNumber: BEBUITextField!
    @IBOutlet weak var txtPassword: BEBUITextField!
    @IBOutlet weak var btnForgotPass: BEBUIButton!
    @IBOutlet weak var btnConfirm: BEBUIButton!
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    func IsValidAllInputs() -> Bool {
        
        if txtPhoneNumber.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtPhoneNumber)
            return false
        }
        
        if txtPassword.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtPassword)
            return false
        }
        
        
        return true
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
   
    @IBAction func continueAction(_ sender: Any) {
        
        if IsValidAllInputs() == false {
            return
        }
        
        self.view.endEditing(true)
        let parameters = RequestParamModal()
        parameters.phoneNumber = txtPhoneNumber.text
        parameters.password = txtPassword.text
        
        RequestManager.singleton.requestGET(requestMethod: kAPILogin, parameters: parameters.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            if let userId = response["id_customers"] {
                
                let currentUser = UserModal(fromDictionary: [:])
                currentUser.userid = userId as! String
                let userData = NSKeyedArchiver.archivedData(withRootObject: currentUser.toDictionary())
                MyUserDefaults.setValue(userData, forKey: kCurrentUser)
            }
            
            NavigationManager.singleton.setupHome()
            
        }) { (response : NSDictionary?, error : Error?) in
            
            if response != nil {
                messageBox(message: "شما باید شماره تلفن اشتباه وارد کنید")
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnForgotPass.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        btnForgotPass.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnForgotPass.layer.shadowOpacity = 1.0
        btnForgotPass.layer.shadowRadius = 10.0
        btnForgotPass.layer.masksToBounds = false
        btnForgotPass.layer.cornerRadius = 5.0
        
        btnConfirm.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        btnConfirm.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnConfirm.layer.shadowOpacity = 1.0
        btnConfirm.layer.shadowRadius = 10.0
        btnConfirm.layer.masksToBounds = false
        btnConfirm.layer.cornerRadius = 5.0
        
        title = "Phone Number".localized()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
