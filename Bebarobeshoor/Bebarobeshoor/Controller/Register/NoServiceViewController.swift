//
//  NoServiceViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/7/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

import UIKit
import Foundation

class NoServiceViewController : UIViewController {
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
