//
//  AddContactViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/3/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Foundation
import GoogleMaps

class AddContactViewController: UITableViewController {
    
    
    @IBOutlet weak var teEXTRestorantNumber: BEBUITextField!

    @IBOutlet weak var txtReferralCode: BEBUITextField!
    @IBOutlet weak var txtFirstName: BEBUITextField!
    @IBOutlet weak var txtLastName: BEBUITextField!
    @IBOutlet weak var txtEmail: BEBUITextField!
    @IBOutlet weak var txtPhoneNumber: BEBUITextField!
    @IBOutlet weak var txtPassword: BEBUITextField!
    @IBOutlet weak var txtConfirmPassword: BEBUITextField!
    @IBOutlet weak var txtArea: BEBUITextField!
    @IBOutlet weak var txtAddressLine: BEBUITextField!
    @IBOutlet weak var txtFlatNumber: BEBUITextField!
    @IBOutlet weak var txtHouseNumber: BEBUITextField!
    @IBOutlet weak var txtPostCode: BEBUITextField!
    @IBOutlet weak var txtFloorNumber: BEBUITextField!
    
    @IBOutlet weak var btnConfirm: BEBUIButton!
    
    var response: GMSReverseGeocodeResponse?
    var isEditProfile : Bool = false
    var address : String?
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func IsValidAllInputs() -> Bool {
        
        if txtFirstName.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtFirstName)
            return false
        }
        
        if txtLastName.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtLastName)
            return false
        }
        
        /*if txtEmail.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtEmail)
            return false
        }
        
        if ValidationManager.singleton.isValidEmail(email: txtEmail.text!.lowercased()) == false {
            showValidFieldMessage(textField: txtEmail)
            return false
        }*/
        
        if txtPhoneNumber.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtPhoneNumber)
            return false
        }
        
        if txtPassword.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtPassword)
            return false
        }
        
        if txtConfirmPassword.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtConfirmPassword)
            return false
        }
        
        if txtConfirmPassword.text != txtPassword.text {
            messageBox(message: "رمز عبور مطابقت ندارد".localized())
            return false
        }
        
        /*let mapAddress : GMSAddress? = response?.results()?.first
        if mapAddress != nil {
            
        } else {
            
            showEmptyFieldMessage(textField: txtAddressLine)
            return false
        }*/
        
        /*if txtArea.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtArea)
            return false
        }*/
        
        if txtAddressLine.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtAddressLine)
            return false
        }
        
        /*if txtFlatNumber.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtFlatNumber)
            return false
        }
        
        if txtHouseNumber.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtHouseNumber)
            return false
        }
        
        if txtHouseNumber.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtHouseNumber)
            return false
        }
        
        if txtPostCode.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtPostCode)
            return false
        }

        if txtFloorNumber.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtFloorNumber)
            return false
        }*/
        return true
    }        
    
    func configureData() {
        
        let mapAddress : GMSAddress? = response?.results()?.first
        
        if (mapAddress != nil) {
            //txtArea.text = mapAddress?.administrativeArea
            txtAddressLine.text = mapAddress?.lines?.joined(separator: " ")
            //txtPostCode.text = mapAddress?.postalCode
        } else {
            txtAddressLine.text = address
        }
        
        if isEditProfile {
            
            txtFirstName.text = currentUser?.firstname
            txtLastName.text = currentUser?.lastname
            txtEmail.text = currentUser?.email
            txtPhoneNumber.text = currentUser?.mobile
            txtReferralCode.isEnabled = false
            
            title = "Edit Profile".localized()
            btnConfirm .setTitle("Save".localized(), for: UIControlState.normal)
            
        } else {
            
            title = "Add Contact Info".localized()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfirmAddressTap(_ sender: BEBUIButton) {
        
        if IsValidAllInputs() == false {
            return
        }
        
        self.view.endEditing(true)
        
        let parameters = RequestParamModal()
        parameters.firstName = txtFirstName.text
        parameters.lastName = txtLastName.text
        //parameters.email = txtEmail.text
        parameters.phoneNumber = txtPhoneNumber.text
        parameters.password = txtPassword.text

        //        parameters.confirmPassword = txtConfirmPassword.text
        //parameters.area = txtArea.text
        
        let mapAddress : GMSAddress? = response?.results()?.first
        if mapAddress != nil {
            parameters.address = mapAddress?.lines?.joined(separator: " ")
        } else if (txtAddressLine.text != nil) {
            parameters.address = txtAddressLine.text
        }
        
        //parameters.flatNumber = txtFlatNumber.text
        //parameters.houseNumber = txtHouseNumber.text
        //parameters.postalCode = txtPostCode.text
        //parameters.floorNumber = txtFloorNumber.text
        parameters.latitude = String(LocationManager.singleton.coordinate2D.latitude)
        parameters.longitude = String(LocationManager.singleton.coordinate2D.longitude)
        
        var isReferralCode = false
        
        if (txtReferralCode.text?.characters.count)! > 0 {
            isReferralCode = true
        }
        
        if isReferralCode == true {
            
            // first of all need to verify referCode
            let requestURL = "api_referral?referralcode=".appending(txtReferralCode.text!)

            RequestManager.singleton.requestGET(requestMethod: requestURL, parameters: [:], showLoader: false, successBlock: { (response : NSDictionary) in
                
                parameters.referralcode = self.txtReferralCode.text
                
                self.requestToVerification(parameters: parameters)
                
            }) { (response : NSDictionary?, error : Error?) in
                
                messageBox(message: "کد مرجع نامعتبر".localized())
                return
            }
            
        } else {
            
            self.requestToVerification(parameters: parameters)
        }
    }
    
    func requestToVerification(parameters : RequestParamModal)  {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.Verification) as! VerificationViewController
        controller.requestParameter = parameters
        
        let requestURL = "http://bebarobeshoor.com/sms/smsapi.php?mobile=0".appending(parameters.phoneNumber)
        
        RequestManager.singleton.requestSendSMS(requestString: requestURL, showLoader: true, successBlock: { (response : NSDictionary) in
            
            if let message = response.object(forKey: "msg") {
                print("code : \(response)" )
                messageBox(message: String(describing: message))
            }
            _ = self.navigationController?.pushViewController(controller, animated: true)
            
        }, failureBlock: { (response : NSDictionary?, error : Error?) in
            
            if let message = response?.object(forKey: "msg") {
                messageBox(message: String(describing: message))
            }
            
            if response?.object(forKey: "code") != nil {
               _ = self.navigationController?.pushViewController(controller, animated: true)
            }
        })
        
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDelegate 
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if isEditProfile == true {
            
            if indexPath.row == 2 ||  indexPath.row == 5 {
                
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnConfirm.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        btnConfirm.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnConfirm.layer.shadowOpacity = 1.0
        btnConfirm.layer.shadowRadius = 3.0
        btnConfirm.layer.masksToBounds = false
        btnConfirm.layer.cornerRadius = 3.0
        
        configureData()

        //txtReferralCode.text = "qsgObB"
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
