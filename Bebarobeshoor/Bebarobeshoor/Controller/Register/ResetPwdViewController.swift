//
//  ResetPwdViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 1/31/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class ResetPwdViewController: BaseNavigationVC {

    @IBOutlet weak var txtPhoneNumber: BEBUITextField!
    @IBOutlet weak var btnCountiue: BEBUIButton!
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func IsValidAllInputs() -> Bool {
        
        if txtPhoneNumber.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtPhoneNumber)
            return false
        }
        
        return true
    }

    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func backAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueAction(_ sender: Any) {
        
        if IsValidAllInputs() == false {
            return
        }
        
        self.view.endEditing(true)
        let parameters = RequestParamModal()
        parameters.phoneNumber = txtPhoneNumber.text
        
        /*RequestManager.singleton.requestGET(requestMethod: kAPIForgotPassword, parameters: parameters.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            messageBox(message: response.value(forKey: "customer_password") as? String ?? "")
            _ = self.navigationController?.popViewController(animated: false)
            
        }) { (response : NSDictionary?, error : Error?) in
            
            if response != nil {
                messageBox(message: "You have enter wrong phone number")
            }
        }*/
        
        let requestURL = "http://bebarobeshoor.com/sms/smsapi.php?mobile=0".appending(parameters.phoneNumber)
        
        let conroller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NewPassword)
        
        RequestManager.singleton.requestSendSMS(requestString: requestURL, showLoader: true, successBlock: { (response : NSDictionary) in
            
            if let message = response.object(forKey: "msg") {
                print("code : \(response)" )
                messageBox(message: String(describing: message))
            }
            _ = self.navigationController?.pushViewController(conroller, animated: true)
            
        }, failureBlock: { (response : NSDictionary?, error : Error?) in
            
            if let message = response?.object(forKey: "msg") {
                messageBox(message: String(describing: message))
            }
            
            if response?.object(forKey: "code") != nil {
                _ = self.navigationController?.pushViewController(conroller, animated: true)
            }
        })
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnCountiue.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        btnCountiue.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnCountiue.layer.shadowOpacity = 1.0
        btnCountiue.layer.shadowRadius = 10.0
        btnCountiue.layer.masksToBounds = false
        btnCountiue.layer.cornerRadius = 5.0
        
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
