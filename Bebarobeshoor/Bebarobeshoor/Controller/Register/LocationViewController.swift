//
//  LocationViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/3/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

//AIzaSyDXmlZ1Lb_NNJH1D4pT1Sz5D-v0W61D9zw
class LocationViewController: BaseNavigationVC, GoogleMapDelegate {

    @IBOutlet weak var btnConfirmAddress: BEBUIButton!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtOptionalAddress: BEBUITextField!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var layoutVerticalPinCenter: NSLayoutConstraint!
    
    var mapView: UIView!
    var response: GMSReverseGeocodeResponse?
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        self.title = "Pin your location on the map".localized()
    }
    
    //------------------------------------------------------
    
    //MARK: GoogleMapDelegate
    
    func mapDidChange() {
        
        UIView.animate(withDuration: 0.3) { 
            self.layoutVerticalPinCenter.constant =  -20
        }
    }
    
    func mapIdleAt(address: String, response: GMSReverseGeocodeResponse) {
        
        self.response = response
        
        UIView.animate(withDuration: 0.3) {
            self.layoutVerticalPinCenter.constant = 0
        }
        txtAddress.text = address
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                break
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfirmTap(_ sender: BEBUIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.AddContact) as! AddContactViewController
        controller.response = response
        
        if txtAddress.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty == true {
            showEmptyFieldMessage(textField: txtAddress)
            return
        }
        
        controller.address = txtAddress.text
        
        if txtOptionalAddress.text?.isEmpty == false {
            controller.address = controller.address?.appending(" ").appending(txtOptionalAddress.text!)
        }
        _ = navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnRecenterTap(_ sender: UIButton) {
    }
    
    @IBAction func btnClearTap(_ sender: UIButton) {
        
        txtAddress.text = ""
        self.view.endEditing(true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }        
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueGoogleMap" {
            
            let controller = segue.destination as! GoogleMapVC
            controller.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnConfirmAddress.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        btnConfirmAddress.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnConfirmAddress.layer.shadowOpacity = 1.0
        btnConfirmAddress.layer.shadowRadius = 10.0
        btnConfirmAddress.layer.masksToBounds = false
        btnConfirmAddress.layer.cornerRadius = 5.0
        
        configureUI()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
