//
//  VerificationViewController.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/3/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Foundation
import IQKeyboardManagerSwift

class VerificationViewController: BaseNavigationVC, UITextFieldDelegate {
    
    var parameters = RequestParamModal()
    var strReferCode:String?
    var isReferCodeEntered = false
    
    @IBOutlet weak var txt1: BEBUITextField!
    @IBOutlet weak var txt2: BEBUITextField!
    @IBOutlet weak var txt3: BEBUITextField!
    @IBOutlet weak var txt4: BEBUITextField!
    @IBOutlet weak var txt5: BEBUITextField!
    
    @IBOutlet weak var lblTimer: BEBUILable!
    @IBOutlet weak var btnCountiue: BEBUIButton!
    
    var timer : Timer!
    let timeInterval:TimeInterval = 1.0
    let timerEnd:TimeInterval = 0.0
    var timeCount:TimeInterval = 59.0
    
    var verification : VerificationModal!
    var requestParameter : RequestParamModal!
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func updateTimer() {
        
        if timeCount > timerEnd {
            timeCount = timeCount - 1
            lblTimer.text = timeString(time: timeCount)
        } else {
            
            timer.invalidate()
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func timeString(time : TimeInterval) -> String {
        
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnBackTap(_ sender: UIBarButtonItem) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        
        let inputCode = "\(txt1.text!)\(txt2.text!)\(txt3.text!)\(txt4.text!)\(txt5.text!)"
        
        //!= verification.oTP
        if (Int(inputCode) == nil) {
            messageBox(message: "لطفا کد امنیتی معتبر را وارد کنید")
            return
        }
        
        RequestManager.singleton.requestGET(requestMethod: kAPISignUp, parameters: requestParameter.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            //id_customers
            if response["id_customers"] is String {
                
                let user = UserModal(fromDictionary: [:])
                user.userid = response["id_customers"] as! String
                print(user.toDictionary())
                let userData = NSKeyedArchiver.archivedData(withRootObject: user.toDictionary())
                MyUserDefaults.setValue(userData, forKey: kCurrentUser)
                
                NavigationManager.singleton.setupHome()
                
            } else {
                
                messageBox(message: "چیزی را اشتباه رفت")
            }
            
        }) { (response : NSDictionary?, error : Error?) in
            
            if let msg = response?.object(forKey: "msg") {
                messageBox(message: String(describing: msg))
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var fullText = textField.text as NSString?
        fullText = fullText?.replacingCharacters(in: range, with: string) as NSString?
        
        if ((fullText?.length)! <= 1) {
            
            if (fullText?.length)! == 1 {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                    IQKeyboardManager.sharedManager().goPrevious()
                })
            }
            return true
            
        } else {
            return false
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCountiue.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        btnCountiue.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnCountiue.layer.shadowOpacity = 1.0
        btnCountiue.layer.shadowRadius = 10.0
        btnCountiue.layer.masksToBounds = false
        btnCountiue.layer.cornerRadius = 5.0

        timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        timer.invalidate()
    }
    
    //------------------------------------------------------
}

