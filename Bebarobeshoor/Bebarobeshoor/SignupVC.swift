//
//  SignupVC.swift
//  Bebarobeshoor
//
//  Created by Dharmesh on 26/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Foundation

class SignupVC : BaseNavigationVC {
    
    @IBOutlet weak var txtFirstName: BEBUITextField!
    @IBOutlet weak var txtLastName: BEBUITextField!
    @IBOutlet weak var txtEmail: BEBUITextField!
    @IBOutlet weak var txtPhoneNumber: BEBUITextField!
    @IBOutlet weak var txtPassword: BEBUITextField!
    @IBOutlet weak var txtConfirmPassword: BEBUITextField!
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func IsValidAllInputs() -> Bool {
        
        if txtFirstName.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtFirstName)
            return false
        }
        
        if txtLastName.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtLastName)
            return false
        }
        
        if txtEmail.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtEmail)
            return false
        }
        
        if ValidationManager.singleton.isValidEmail(email: txtEmail.text!.lowercased()) == false {
            showValidFieldMessage(textField: txtEmail)
            return false
        }
        
        if txtPhoneNumber.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtPhoneNumber)
            return false
        }
        
        if txtPassword.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtPassword)
            return false
        }
        
        if txtConfirmPassword.text?.isEmpty == true {
            showEmptyFieldMessage(textField: txtConfirmPassword)
            return false
        }
        
        if txtConfirmPassword.text != txtPassword.text {
            messageBox(message: "رمز عبور مطابقت ندارد")
            return false
        }
        
        return true
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnSignUpTap(_ sender: UIButton) {

        if IsValidAllInputs() == false {
            return
        }

        self.view.endEditing(true)
        let parameters = RequestParamModal()
        parameters.firstName = txtFirstName.text
        parameters.lastName = txtLastName.text
        parameters.email = txtEmail.text
        parameters.phoneNumber = txtPhoneNumber.text
        parameters.password = txtPassword.text
        parameters.confirmPassword = txtConfirmPassword.text
        parameters.latitude = String(LocationManager.singleton.coordinate2D.latitude)
        parameters.longitude = String(LocationManager.singleton.coordinate2D.longitude)
        
        RequestManager.singleton.requestGET(requestMethod: kAPISignUp, parameters: parameters.toDictionary() as! [String : AnyObject], showLoader: true, successBlock: { (response : NSDictionary) in
            
            let verification = VerificationModal(fromDictionary: response)
            let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.Verification) as! VerificationViewController
            controller.verification = verification
            //messageBox(message: "Verification code : \(verification.oTP)")            
            let requestURL = "http://bebarobeshoor.com/sms/smsapi.php?mobile=0".appending(parameters.phoneNumber)
            
            RequestManager.singleton.requestSendSMS(requestString: requestURL, showLoader: true, successBlock: { (response : NSDictionary) in
                
                _ = self.navigationController?.pushViewController(controller, animated: true)
                
            }, failureBlock: { (response : NSDictionary?, error : Error?) in
                
            })
            
        }) { (response : NSDictionary?, error : Error?) in
            
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
