//
//  SummaryCell.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/5/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell {

    @IBOutlet weak var lblTitle: BEBUILable!
    @IBOutlet weak var lblPrice: BEBUILable!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
