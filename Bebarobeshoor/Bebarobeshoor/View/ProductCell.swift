//
//  ProductCell.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/4/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import GMStepper
import PopupDialog

class ProductCell: UICollectionViewCell {
    
    var rootVC:  PricingViewController? = nil
    
    @IBOutlet weak var stepper: GMStepper!
    @IBOutlet weak var imvPercentCorner: UIImageView!
    @IBOutlet weak var imvInfo: UIImageView!
    @IBOutlet weak var imvCheckCorner: UIImageView!
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    
    var product : ProductModal!
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func stepperValueDidChange(stepper: GMStepper) {
        
        if stepper.value == 0.0 {
            unMarkAsSelectedProduct()
            ProductManager.singleton.removeProduct(product: product)
        } else {
            markAsSelectedProduct()
            ProductManager.singleton.addProduct(product: product)
        }
        product.tickCount = stepper.value
        rootVC?.tblProduct.reloadData()
    }
    
    func markAsSelectedProduct() {
        
        imvCheckCorner.isHidden = false
        self.contentView.layer.borderColor = kColorGreen.cgColor
        self.contentView.layer.borderWidth = 1.0
    }
    
    func unMarkAsSelectedProduct() {
        
        imvCheckCorner.isHidden = true
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.borderWidth = 0.0
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnInfoTap(_ sender: UIButton) {
        
        /*guard rootVC == nil else {
            rootVC?.showInfoView()
            return
        }*/
        
        let title = product.price
        let message = product.title
        let image = imgProduct.image
        
        let popup = PopupDialog(title: title, message: message, image: image)
        
        let buttonOne = CancelButton(title: kOk) {
        }
        
        popup.addButtons([buttonOne])
        rootVC?.present(popup, animated: true, completion: nil)
        
    }
    
    //------------------------------------------------------
    
    //MARK: Initialisation 
    
    override func prepareForReuse() {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        stepper.addTarget(self, action: #selector(stepperValueDidChange), for: UIControlEvents.valueChanged)
    }
    
    //------------------------------------------------------
    
}
