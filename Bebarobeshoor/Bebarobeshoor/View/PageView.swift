//
//  PageView.swift
//  Bebarobeshoor
//
//  Created by Developer on 1/31/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class PageView: UIView {

    @IBOutlet weak var imvBg: UIImageView!
    @IBOutlet weak var imvIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAbout: UILabel!    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
}
