//
//  UIController.swift
//  Bebarobeshoor
//
//  Created by Dharmesh on 12/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation
import UIKit
import Localize_Swift

let kFontNika = "Nika-Regular"
let kButtonDefaultSize : CGFloat = 17.0
let kTextFieldDefaultSize : CGFloat = 15.0
let kLabelDefaultSize : CGFloat = 15.0

class BEBUIButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = UIFont(name: kFontNika, size:self.titleLabel?.font?.pointSize ?? kButtonDefaultSize)
        
        let keyNormal = self.title(for: .normal)
        let keyHighlighted = self.title(for: .highlighted)
        let keySelected = self.title(for: .selected)
        let keyDisable = self.title(for: .disabled)
        
        if keyNormal != nil {
            self .setTitle(keyNormal!.localized(), for: .normal)
        }
        
        if keyHighlighted != nil {
            self .setTitle(keyNormal!.localized(), for: .highlighted)
        }
        
        if keySelected != nil {
            self .setTitle(keyNormal!.localized(), for: .selected)
        }
        
        if keyDisable != nil {
            self .setTitle(keyNormal!.localized(), for: .disabled)
        }
    }
}

class BEBUITextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.font = UIFont(name: kFontNika, size:self.font?.pointSize ?? kTextFieldDefaultSize)
        self.placeholder = self.placeholder?.localized()
        self.textAlignment = .right
        
        if self.tag == 100 || self.tag == 101{
           
        }
        
        /*if (self.baseWritingDirection(for: self.beginningOfDocument, in: UITextStorageDirection.forward)) == .rightToLeft {
            self.textAlignment = .right
        } else {
            self.textAlignment = .left
        }*/
    }
}

class BEBUILable: UILabel {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.font = UIFont(name: kFontNika, size:self.font?.pointSize ?? kLabelDefaultSize)
        self.text = self.text?.localized()
    }
}
