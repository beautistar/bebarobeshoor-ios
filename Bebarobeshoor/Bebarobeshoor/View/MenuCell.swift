//
//  MenuCell.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/2/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    class var identifier: String { return String.className(self) }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 48
    }
    
    open func setData(_ data: Any?) {
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
//        self.backgroundColor = UIColor(hex: "F1F8E9")
        self.backgroundColor = UIColor.clear
//        self.textLabel?.font = UIFont.italicSystemFont(ofSize: 18)
//        self.textLabel?.textColor = UIColor(hex: "9E9E9E")
        self.textLabel?.textColor = UIColor.white
        if let menuText = data as? String {
            self.textLabel?.text = menuText
        }
    }
    
    override open func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            self.alpha = 0.4
        } else {
            self.alpha = 1.0
        }
    }
}
