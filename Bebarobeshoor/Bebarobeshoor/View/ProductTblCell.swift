//
//  ProductTblCell.swift
//  Bebarobeshoor
//
//  Created by Developer on 2/5/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import GMStepper

class ProductTblCell: UITableViewCell {

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var stepper: GMStepper!
    
    var rootVC:  PricingViewController? = nil
    var product : ProductModal!
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func stepperValueDidChange(stepper: GMStepper) {
        
        if stepper.value == 0.0 {
            ProductManager.singleton.removeProduct(product: product)
        } else {
            ProductManager.singleton.addProduct(product: product)
        }
        product.tickCount = stepper.value
        rootVC?.updateProduct()
        rootVC?.tblProduct.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        stepper.addTarget(self, action: #selector(stepperValueDidChange), for: UIControlEvents.valueChanged)
    }
}
