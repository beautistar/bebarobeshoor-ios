//
//  GoogleMapVC.swift
//  Bebarobeshoor
//
//  Created by Dharmesh on 12/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Foundation
import GoogleMaps

protocol GoogleMapDelegate {
    
    func mapDidChange()
    func mapIdleAt(address : String, response : GMSReverseGeocodeResponse)
}

class GoogleMapVC : UIViewController, GMSMapViewDelegate, LocationManagerDelegate {
    
    var delegate : GoogleMapDelegate?
    var mapView: GMSMapView!
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func loadView() {
   
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        
        mapView.settings.myLocationButton = true
        mapView.setMinZoom(10, maxZoom: 15)
        mapView.delegate = self
        view = mapView
        
        // Creates a marker in the center of the map.
        /*let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView*/
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        delegate?.mapDidChange()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
                
        GMSGeocoder().reverseGeocodeCoordinate(position.target) { (response : GMSReverseGeocodeResponse?, error : Error?) in
            
            if error == nil && response != nil {
                let address = response?.firstResult()
                self.delegate?.mapIdleAt(address: (address?.lines?.joined(separator: " ")) ?? "", response: response!)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: LocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            LocationManager.singleton.stopMonitoring()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocationManager.singleton.delegate = self
        LocationManager.singleton.startMonitoring()
        mapView.isMyLocationEnabled = true
        //mapView.settings.myLocationButton = true
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

